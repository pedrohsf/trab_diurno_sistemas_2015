#Sobre os mockups

## Que software eu devo utilizar?

Temos vários softwares lá fora. Mas o que eu recomendo agora é o Evolus Pencil. Ele é *simples*, funcional e ele é aparentemente estável.

Também temos outras opções como

 * Balsamiq Mockups,
 * Mockingbird,
 * Mockup builder,
 * Mockflow,
 * HotGloo,
 * Mais [nesse site](http://mashable.com/2012/06/07/mockup-tools/) e [nesse site também](http://alternativeto.net/software/balsamiq-mockups/).

Tenta utilizar uma opção open source para não se restringir a normas.

## Como devo fazer mockups?

Mockups é um meio de prototipação. Você **não** criará telas funcionais com isso. Mockups é utilizado principalmente para criar conceitos para uma possível implementação.

Ao fazer mockups, seja realista, não foge muito do que já foi criado, e tenha bom senso. Pense sempre no seu usuário final.
