
 package Data;

import business.Pessoajuridica;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PessoaJuridicaData {

    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectPessoaJuridica() {
        List<Pessoajuridica> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Pessoajuridica");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
           
            e.printStackTrace();
            return new ArrayList<Pessoajuridica>();
        }
        return lista;
    }

    public String savePessoaJuridica(Pessoajuridica pessoaJuridica) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(pessoaJuridica);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deletePessoajuridica(Pessoajuridica pessoaJuridica) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(pessoaJuridica);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;

    }
    
    public Pessoajuridica getPessoajuridica(int idPessoajuridica) {
        List<Pessoajuridica> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Pessoajuridica where idPessoajuridica = " + idPessoajuridica);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            
            e.printStackTrace();
            return null;
        }
        
    }
}
