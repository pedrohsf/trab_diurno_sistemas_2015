package Data;

import business.Itemcompra;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ItemCompraData {
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectItemcompra() {
        List<Itemcompra> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Itemcompra");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Itemcompra>();
        }
        return lista;
    }

    public String insertUpdateItemcompra(Itemcompra itemcompra) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(itemcompra);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteItemcompra(Itemcompra itemcompra) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(itemcompra);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }
    
    public String saveItemcompra(Itemcompra itemcompra) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(itemcompra);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public Itemcompra getItemvenda(int idCompra, int Produto) {
        List<Itemcompra> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Itemcompra where idCompra = " + idCompra + " and idProduto = " + Produto);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } 
    }    
}
