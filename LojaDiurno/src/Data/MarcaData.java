package Data;

import business.Marca;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class MarcaData {
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectMarca() {
        List<Marca> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Marca");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Marca>();
        }
        return lista;
    }

    public String saveMarca(Marca marca) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(marca);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteMarca(Marca marca) {
        String erro = null;
        try { 
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(marca);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Marca getMarca(int idMarca) {
        List<Marca> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Marca where idMarca = " + idMarca);
            lista = query.list();
            sessao.close(); 
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    } 
    
}
