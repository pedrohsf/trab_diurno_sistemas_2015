package Data;

import business.Tipopagamento;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TipoPagamentoData {
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectTipoPagamento() {
        List<Tipopagamento> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Tipopagamento");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Tipopagamento>();
        }
        return lista;
    }

    public String insertUpdateTipopagamento(Tipopagamento tipoPagamento) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(tipoPagamento);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteTipoPagamento(Tipopagamento tipoPagamento) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(tipoPagamento);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }
    
    public String saveTipoPagamento(Tipopagamento tipoPagamento) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(tipoPagamento);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

   public Tipopagamento getTipoPagamento(int IdTipoPagamento) {
        List<Tipopagamento> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Tipopagamento where IdTipoPagamento = " + IdTipoPagamento);
            lista = query.list();
            sessao.close(); 
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    }
}
