
package Data;

import business.Itemproduto;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class ItemProdutoData {
    
    
      
     private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectItemProduto() {
        List<Itemproduto> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from ItemProduto");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Itemproduto>();
        }
        return lista;
    }

    public String saveItemProduto(Itemproduto itemProduto) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(itemProduto);
            transacao.commit();
            sessao.close(); 
             
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteItemProduto(Itemproduto itemProduto) {
        String erro = null;
        try { 
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(itemProduto);
            transacao.commit(); 
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Itemproduto getItemProduto(int idItemProduto) {
        List<Itemproduto> lista = new ArrayList();
        try { 
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Itemproduto where idItemProduto = " + idItemProduto);
            lista = query.list();
            sessao.close();  
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    } 
    
    
}
