package Data;

import business.Endereco;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EnderecoData {
    
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectEndereco() {
        List<Endereco> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Endereco");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Endereco>();
        }
        return lista;
    }

    public String saveEndereco(Endereco endereco) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(endereco);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteEndereco(Endereco endereco) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(endereco);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Endereco getEndereco(int idEndereco) {
        List<Endereco> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Endereco where idEndereco = " + idEndereco);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }
    

    public Endereco getEnderecoCep(String cep) {
        List<Endereco> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Endereco e JOIN FETCH e.bairro JOIN FETCH e.bairro.cidade JOIN FETCH e.bairro.cidade.estado where e.cep like '%" + cep + "%'");
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }
}
