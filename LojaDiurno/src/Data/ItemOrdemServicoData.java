package Data;

import business.Itemordemservico;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class ItemOrdemServicoData {
     private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectItemOrdemServico() {
        List<Itemordemservico> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Itemordemservico");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Itemordemservico>();
        }
        return lista;
    }

    public String insertUpdateItemOrdemServico(Itemordemservico itemOrdemServico) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(itemOrdemServico);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteItemOrdemServico(Itemordemservico itemOrdemServico) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(itemOrdemServico);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }
    
    public String saveItemOrdemServico(Itemordemservico itemOrdemServico) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(itemOrdemServico);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public Itemordemservico getItemOrdemServico(int IdOrdemServico, int IdProduto) {
        List<Itemordemservico> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Itemordemservico where IdOrdemServico = " + IdOrdemServico + " and idProduto = " + IdProduto);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } 
    }    
}
