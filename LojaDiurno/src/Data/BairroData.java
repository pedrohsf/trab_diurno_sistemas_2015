package Data;

import business.Bairro;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class BairroData {
    
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectBairro() {
        List<Bairro> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Bairro");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
           
            e.printStackTrace();
            return new ArrayList<Bairro>();
        }
        return lista;
    }

    public String saveBairro(Bairro bairro) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(bairro);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deleteBairro(Bairro bairro) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(bairro);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;

    }
    
    public Bairro getBairro(int idBairro) {
        List<Bairro> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Bairro where idBairro = " + idBairro);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            
            e.printStackTrace();
            return null;
        }
        
    }
    
}
