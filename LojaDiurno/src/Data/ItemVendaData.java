

package Data;

import business.Itemvenda;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class ItemVendaData {
       
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectItemvenda() {
        List<Itemvenda> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Itemvenda");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Itemvenda>();
        }
        return lista;
    }

    public String insertUpdateItemvenda(Itemvenda itemvenda) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(itemvenda);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteItemvenda(Itemvenda itemvenda) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(itemvenda);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }
    
    public String saveItemvenda(Itemvenda itemvenda) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(itemvenda);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public Itemvenda getItemvenda(int idVenda, int Produto) {
        List<Itemvenda> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Itemvenda where idVenda = " + idVenda + " and idProduto = " + Produto);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } 
    }
}
