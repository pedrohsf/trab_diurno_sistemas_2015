
package Data;

import business.Produto;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class ProdutoData {
     
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectProduto() {
        List<Produto> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Produto");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Produto>();
        }
        return lista;
    }
    
    public List selectProduto(String descricao) {
        List<Produto> lista = new ArrayList<Produto>();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Produto where descricao like '%" + descricao + "%'");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Produto>();
        }
        return lista;
    }

    public String saveProduto(Produto produto)
    {
        String erro = null;
        try
        { 
                this.sessao = Sessao.getSessao();
                transacao = sessao.beginTransaction();
                sessao.saveOrUpdate(produto);
                transacao.commit();
                sessao.close();          
          }
        catch (HibernateException e)
        {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteProduto(Produto produto) {
        String erro = null;
        try { 
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(produto);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Produto getProduto(int idProduto) {
        List<Produto> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Produto where idProduto = " + idProduto);
            lista = query.list();
            sessao.close(); 
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    }
    public List<Produto> getProduto(String descricao)
    {
         List<Produto> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Produto where descricao like ?");
            query.setString(0, "%" + descricao + "%");
            lista = query.list();
            sessao.close(); 
            return lista;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    }
}
