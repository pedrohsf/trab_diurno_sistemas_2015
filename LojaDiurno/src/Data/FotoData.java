
package Data;

import business.Foto;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class FotoData {
    
     private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectFoto() {
        List<Foto> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Foto F LEFT JOIN FETCH F.produto");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Foto>();
        }
        return lista;
    }

    public String saveFoto(Foto foto) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(foto);
            transacao.commit();
            sessao.close(); 
             
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteFoto(Foto foto) {
        String erro = null;
        try { 
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(foto);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Foto getFoto(int idFoto) {
        List<Foto> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Foto where idFoto = " + idFoto);
            lista = query.list();
            sessao.close();  
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    } 
    
    public List<Foto> getFoto(String nome)
    {
         List<Foto> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Foto where NomeFisico like ?");
            query.setString(0, "%" + nome + "%");
            lista = query.list();
            sessao.close(); 
            return lista;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    }
    
}
