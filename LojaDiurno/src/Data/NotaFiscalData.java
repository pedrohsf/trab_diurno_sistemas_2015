
package Data;

import business.Notafiscal;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class NotaFiscalData {
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectNotaFiscal() {
        List<Notafiscal> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Notafiscal");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Notafiscal>();
        }
        return lista;
    }

    public String insertUpdateNotaFiscal(Notafiscal notaFiscal) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(notaFiscal);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteNotaFiscal(Notafiscal notaFiscal) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(notaFiscal);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }
    
    public String saveNotaFiscal(Notafiscal notaFiscal) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(notaFiscal);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

   public Notafiscal getNotaFiscal(int IdNotaFiscal) {
        List<Notafiscal> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Notafiscal where IdNotaFiscal = " + IdNotaFiscal);
            lista = query.list();
            sessao.close(); 
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    }
}
