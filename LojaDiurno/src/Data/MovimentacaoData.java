package Data;

import business.Marca;
import business.Movimentacao;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class MovimentacaoData
{
    private Session sessao;
    private Transaction transacao;
    private Query query;
    
    public List selectMovimentacao()
    {
        List <Movimentacao> lista = new ArrayList();
        try
        {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Movimentacao");
            lista = query.list();
            sessao.close();
        }
        catch(HibernateException ex)
        {
            ex.printStackTrace();
            return new ArrayList<Movimentacao>();
        }
        return lista;
    }
    
    public List selectMovimentacaoEntrada(int idProduto)
    {
        List<Movimentacao> lista = new ArrayList();
        
        try
        {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Movimentacao mov join fetch mov.tipomovimentacao tm join fetch mov.produto p where tm.idTipoMovimentacao = "
                    + 1 + " and p.idProduto = " + idProduto);
            lista = query.list();
            sessao.close(); 
            return lista;
        }
        
        catch (HibernateException e)
        {
            e.printStackTrace();
            return null; 
        }
    }
    
    public List selectMovimentacaoSaida(int idProduto)
    {
        List<Movimentacao> lista = new ArrayList();
        
        try
        {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Movimentacao mov join fetch mov.tipomovimentacao tm join fetch mov.produto p where tm.idTipoMovimentacao = " + 2 + " and p.idProduto = " + idProduto);
            lista = query.list();
            sessao.close(); 
            return lista;
        }
        
        catch (HibernateException e)
        {
            e.printStackTrace();
            return null; 
        }
    }
    
    public String saveMovimentacao(Movimentacao movimentacao)
    {
        String erro = null;
        
        try
        {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(movimentacao);
            transacao.commit();
            sessao.close();
            
        }
        
        catch (HibernateException e)
        {
            erro = e.getMessage();
            e.printStackTrace();
        }
        
        return erro;
    }
    
    public String deleteMovimentacao(Movimentacao movimentacao)
    {
        String erro = null;
        
        try
        { 
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(movimentacao);
            transacao.commit();
            sessao.close();

        }
        
        catch (HibernateException e)
        {
            erro = e.getMessage();
            e.printStackTrace();
        }
        
        return erro;
    }
    
    public Movimentacao getMovimentacao(int idMovimentacao) {
        List<Movimentacao> lista = new ArrayList();
        
        try
        {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Movimentacao where idMovimentacao = " + idMovimentacao);
            lista = query.list();
            sessao.close(); 
            return lista.get(0);
        }
        
        catch (HibernateException e)
        {
            e.printStackTrace();
            return null; 
        }
    } 
    
}
