package Data;

import business.Tipomovimentacao;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TipoMovimentacaoData {
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectTipoMovimentacao() {
        List<Tipomovimentacao> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Tipomovimentacao");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Tipomovimentacao>();
        }
        return lista;
    }

    public String insertUpdateTipoMovimentacao(Tipomovimentacao tipoMovimentacao) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(tipoMovimentacao);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteTipoMovimentacao(Tipomovimentacao tipoMovimentacao) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(tipoMovimentacao);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }
    
    public String saveTipoMovimentacao(Tipomovimentacao tipoMovimentacao) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(tipoMovimentacao);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

   public Tipomovimentacao getTipoMovimentacao(int IdTipoMovimentacao) {
        List<Tipomovimentacao> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Tipomovimentacao where IdTipoMovimentacao = " + IdTipoMovimentacao);
            lista = query.list();
            sessao.close(); 
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null; 
        }
    }
}
