package UI;

import Data.BairroData;
import Data.CidadeData;
import Data.EnderecoData;
import Data.EnderecoPessoaData;
import Data.TelefoneData;
import Data.TipoTelefoneData;
import Data.VendedorData;
import business.Bairro;
import business.Cidade;
import business.Endereco;
import business.Enderecopessoa;
import business.EnderecopessoaId;
import business.Pessoa;
import business.Pessoafisica;
import business.Pessoajuridica;
import business.Telefone;
import business.Tipotelefone;
import business.Vendedor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class FormFuncionario extends javax.swing.JFrame {

    private int idVendedor;
    private int idEndereco;
    private Integer idTelefone = null;
    private Vendedor vendedor;
    private Enderecopessoa enderecoPessoa;
    private Endereco endereco;
    private VendedorData vendedorData;
    private EnderecoData enderecoData;
    private EnderecoPessoaData enderecopessoaData;
    private BairroData bairroData;
    private CidadeData cidadeData;
    private TelefoneData telefoneData;
    private TipoTelefoneData tipoTelefoneData;
    private Tipotelefone tipotelefone;
    private Telefone telefone;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    public FormFuncionario() {
        initComponents();
        tabPrincipal.setEnabledAt(1,false);
        tabPrincipal.setEnabledAt(2,false);
                
        vendedorData = new VendedorData();
        enderecoData = new EnderecoData();
        enderecopessoaData = new EnderecoPessoaData();
        telefoneData = new TelefoneData();
        bairroData = new BairroData();
        cidadeData = new CidadeData();
        tipoTelefoneData = new TipoTelefoneData();
        
        atualizarTabela();
        
        TipoTelefoneData ttd = new TipoTelefoneData();
        cbxTipoTelefone.removeAllItems();
        for (Tipotelefone t : ttd.selectTipotelefone()){
            cbxTipoTelefone.addItem(t.getDescricao());
        }
        
        idTelefone = 0;
        
    }
    
    private void atribuirVendedor(Vendedor v)
    {
        idVendedor = v.getIdVendedor();
        txtNome.setText(v.getPessoa().getNome());
        txtEmail.setText(v.getPessoa().getEmail());
        txtCpf.setText(v.getPessoa().getPessoafisica().getCpf());
        txtObservacao.setText(v.getPessoa().getObservacao());
        txtDataNasc.setText(dateFormat.format(v.getPessoa().getPessoafisica().getDataNascimento()));
        txtRg.setText(v.getPessoa().getPessoafisica().getRg());
        if(v.getPessoa().getPessoafisica().getSexo() == 'M') {
            rdoMasc.setSelected(true);
        }
        else if(v.getPessoa().getPessoafisica().getSexo() == 'F') {
            rdoFem.setSelected(true);
        }
    }
     
     private void obterVendedor(){
         Pessoa p = new Pessoa();
         
         p.setIdPessoa(idVendedor);
         
         p.setVendedor(vendedor);
         p.setNome(txtNome.getText());
         p.setEmail(txtEmail.getText());
         p.setObservacao(txtObservacao.getText());
         
         Pessoafisica pf = new Pessoafisica();
         pf.setCpf(txtCpf.getText());
         pf.setIdPessoaFisica(idVendedor);
         pf.setRg(txtRg.getText());
         Date dataNascimento;
         try {
            dataNascimento = dateFormat.parse(txtDataNasc.getText());
         } catch(Exception ex) {
             dataNascimento = new Date();
         }
         pf.setDataNascimento(dataNascimento);
         if(rdoMasc.isSelected()) {
             pf.setSexo('M');
         } else {
             pf.setSexo('F');
         }
         
         p.setPessoafisica(pf);
         
         vendedor = new Vendedor(p);
         
         vendedor.setIdVendedor(idVendedor);
         
         if (idVendedor == 0) {
            vendedor.setIdVendedor(null);
            vendedor.getPessoa().setIdPessoa(null);
            vendedor.getPessoa().getPessoafisica().setIdPessoaFisica(null);
         }
         
         
      }
     private void obterEndereco(){
         
         enderecoPessoa = new Enderecopessoa();
         enderecoPessoa.setId(new EnderecopessoaId(endereco.getIdEndereco(), vendedor.getIdVendedor()));
         enderecoPessoa.setPessoa(vendedor.getPessoa());
         enderecoPessoa.setEndereco(endereco);
         enderecoPessoa.setComplemeto(txtComplemento.getText());
         enderecoPessoa.setNumero(Short.parseShort(txtNumero.getText()));
         enderecoPessoa.setObservacao(txtObservacao.getText());
                  
     }
     private void atribuirEndereco(Enderecopessoa e){
         idEndereco = e.getId().getIdEndereco();
         txtComplemento.setText(e.getComplemeto());
         txtNumero.setText(String.valueOf(e.getNumero()));
         txtObservacao.setText(e.getObservacao());
         txtLogradouro.setText(e.getEndereco().getEnderecoCompleto()); //se for getLogradouro() ele pega só "Rua" ao invés de "Rua Tal..."
         txtCEP.setText(e.getEndereco().getCep());
         txtCidade.setText(e.getEndereco().getBairro().getCidade().getCidade());
         txtBairro.setText(e.getEndereco().getBairro().getBairro());
         cbxUF.setSelectedItem(e.getEndereco().getBairro().getCidade().getEstado().getUf());
     }
     
    
    private boolean validar()
    {
        if(txtNome.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "Nome não informado!", "Nome não foi Informado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(txtEmail.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "Email não informado!", "Email não informado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        if(txtCpf.getText().replace(".","").replace("-","").replace(" ","").equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "CPF não informado!", "CPF não informado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(txtDataNasc.getText().replace("/","").replace(" ","").equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "Data de Nascimento não informada!", "Data de Nascimento não informada", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(txtRg.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "RG não informada!", "RG não informada", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(!rdoFem.isSelected() && !rdoMasc.isSelected())
        {
            JOptionPane.showMessageDialog(txtComplemento, "Sexo não informaoa!", "Sexo não informada", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        return true;
    }
    private boolean validarTelefone(){
        if(tabPrincipal.isEnabledAt(1) == true)
        {
            if(txtNumeroTelefone.getText().replace("(","").replace(")","").replace("-","").replace(" ","").equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O numero telefônico não foi informado.", "Numero teleônico não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if(txtOperadora.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "A operadora do telefone não foi informado.", "Operadora não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
        }
     return true;
    }
    
    private Telefone obterTelefone()
    {
        Telefone telefone= new Telefone();
        //telefone.setIdTelefone(new TelefoneId(telefone.getIdTelefone(), fornecedor.getIdFornecedor()));
        TipoTelefoneData telefoneData = new TipoTelefoneData();
        List<Tipotelefone> t = telefoneData.selectTipotelefone();
        Tipotelefone tf = new Tipotelefone();
        for(Tipotelefone tt : t)
        {
            if(tt.getDescricao().equals(cbxTipoTelefone.getSelectedItem().toString()))
            {
                tf = tt;
            }
        }
        
        
        vendedor.getPessoa().setIdPessoa(vendedor.getIdVendedor());
        
        telefone.setPessoa(vendedor.getPessoa());
        telefone.setNumeroTelefone(txtNumeroTelefone.getText());
        telefone.setOperadora(txtOperadora.getText());
        //telefone.setTipotelefone((Tipotelefone)cbxTipoTelefone.getSelectedItem());
        telefone.setTipotelefone(tf);
        if (idTelefone == 0) {
            telefone.setIdTelefone(null);
        } else {
            telefone.setIdTelefone(idTelefone);
        }
        
        return telefone;
        
    }
    
    private void salvarTelefone(Telefone telefone)
    {
        telefoneData.saveTelefone(telefone);
    }
    
    public void atribuirTelefone(Telefone t)
    {
        txtNumeroTelefone.setText(t.getNumeroTelefone());
        txtOperadora.setText(String.valueOf(t.getOperadora()));
        //Tipotelefone item = tipoTelefoneData.getTipotelefone(telefone.getTipotelefone().getIdTipoTelefone());
        cbxTipoTelefone.setSelectedItem(telefone.getTipotelefone().getDescricao());
    }
    
    public void limparTelefone()
    {
        txtNumeroTelefone.setText("");
        txtOperadora.setText("");
        cbxTipoTelefone.setSelectedIndex(0);
    }
    
    private boolean validarEndereco(){
        
        if(tabPrincipal.isEnabledAt(2) == true)
        {
            txtLogradouro.setEnabled(false);
            txtBairro.setEnabled(false);
            txtCidade.setEnabled(false);
            cbxUF.setEnabled(false);
            if(txtCEP.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "CEP deve ser informado.", "CEP não foi informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtNumero.getText().equals("") )
            {
                JOptionPane.showMessageDialog(txtComplemento, "Numero do endereço deve ser informado.", "Numero do Endereço não infomado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtLogradouro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O logradouro deve ser informado.", "Logradouro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtBairro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O Bairro deve ser informado.", "Bairro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtCidade.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "A cidade deve ser informado.", "Cidade não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(cbxUF.getSelectedIndex() == 0)
            {
                JOptionPane.showMessageDialog(txtComplemento, "A unidade federativa ou sigla do estado deve ser informado.", "Unidade Federativa não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
        }
        
        return true;
    }
    
    private void limparEndereco(){
         txtComplemento.setText("");
         txtNumero.setText("");
         txtObservacao.setText("");
         txtLogradouro.setText("");
         txtCEP.setText("");
         txtCidade.setText("");
         txtBairro.setText("");
         cbxUF.setSelectedItem(0);
    }
    
    private void limparFuncionario(){
         txtNome.setText("");
         txtEmail.setText("");
         txtDataNasc.setText("");
         buttonGroup1.clearSelection();
         txtRg.setText("");
         txtCpf.setText("");
         txtObservacao.setText("");
         
         idVendedor = 0;
         vendedor = new Vendedor();
    }
    
    public void atualizarTabela() {
        tblVendedor = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Nome", "CPF"}, 0) {
            private static final long serialVersionUID = 1L;
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        //tblFornecedor.setModel();
        model.setRowCount(0);
        List<Vendedor> vendedores = vendedorData.selectVendedor();
        for (Vendedor v : vendedores) {
            
            model.addRow(new Object[]{ 
                v, 
                v.getPessoa().getNome(),
                v.getPessoa().getPessoafisica().getCpf()
            });
        }
        tblVendedor.setModel(model);
       
        tblVendedor.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnEditarActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        tblVendedor.setAutoCreateRowSorter(true);
        tblVendedor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblVendedor.getColumnModel().getColumn(0).setMinWidth(0);
        tblVendedor.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblVendedor.getColumnModel().getColumn(1).setPreferredWidth(280);
        tblVendedor.getColumnModel().getColumn(2).setPreferredWidth(150);
        tblVendedor.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane4.setViewportView(tblVendedor);
    }
    
    public void atualizarTabelaEndereco() {
        tblVendedorEndereco = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Logradouro", "Numero", "Bairro", "Cidade"}, 0) {
            private static final long serialVersionUID = 1L;
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        //tblFornecedor.setModel();
        model.setRowCount(0);
        List<Object[]> enderecos = enderecopessoaData.selectPorPessoa(vendedor.getIdVendedor());
        for (Object[] o : enderecos) {
            Enderecopessoa ep = (Enderecopessoa)o[0];
            Endereco e = (Endereco)o[1];
            Bairro b = (Bairro)o[2];
            Cidade c = (Cidade)o[3];
            //Bairro b = bairroData.getBairro(e.getBairro());
            model.addRow(new Object[]{ 
                ep, 
                e.getEnderecoCompleto(), //.getLogradouro mostrou só "Rua", e não "Rua Tal..."
                ep.getNumero(),
                b.getBairro(),
                c.getCidade()
            });
        }
        tblVendedorEndereco.setModel(model);
       
        tblVendedorEndereco.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnEditarEnderecoActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        tblVendedorEndereco.setAutoCreateRowSorter(true);
        tblVendedorEndereco.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblVendedorEndereco.getColumnModel().getColumn(0).setMinWidth(0);
        tblVendedorEndereco.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblVendedorEndereco.getColumnModel().getColumn(1).setPreferredWidth(250);
        tblVendedorEndereco.getColumnModel().getColumn(2).setPreferredWidth(150);
        tblVendedorEndereco.getColumnModel().getColumn(3).setPreferredWidth(250);
        tblVendedorEndereco.getColumnModel().getColumn(4).setPreferredWidth(240);
        tblVendedorEndereco.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane8.setViewportView(tblVendedorEndereco);
    }
    
    public void atualizarTabelaTelefone() {
        tblVendedorTelefone = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"","","Telefone", "Operadora", "Tipo de Telefone"}, 0) {
            //1 telefone
            //2 tipo telefone
                       
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        
        
        model.setRowCount(0);
        //Object[] telefones = telefoneData.selectPorPessoa(cliente.getIdCliente()).toArray();
        
        List<Object[]> telefones = telefoneData.selectPorPessoa(vendedor.getIdVendedor());
        
        for (Object[] o : telefones)
        {
            Telefone t = (Telefone)o[0];
            Tipotelefone tp = (Tipotelefone)o[1];
            
            model.addRow(new Object[]{ 
                t,
                tp,
                t.getNumeroTelefone(),
                t.getOperadora(),
                tp.getDescricao()
            });
        }
        
        tblVendedorTelefone.setModel(model);
       
        tblVendedorTelefone.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnEditarTelefoneActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        
        tblVendedorTelefone.setAutoCreateRowSorter(true);
        tblVendedorTelefone.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblVendedorTelefone.getColumnModel().getColumn(0).setMinWidth(0);
        tblVendedorTelefone.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblVendedorTelefone.getColumnModel().getColumn(1).setMinWidth(0);
        tblVendedorTelefone.getColumnModel().getColumn(1).setPreferredWidth(0);
        tblVendedorTelefone.getColumnModel().getColumn(2).setPreferredWidth(280);
        tblVendedorTelefone.getColumnModel().getColumn(3).setPreferredWidth(200);
        tblVendedorTelefone.getColumnModel().getColumn(4).setPreferredWidth(200);
        tblVendedorTelefone.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane7.setViewportView(tblVendedorTelefone);
        
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        tabPrincipal = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        label4 = new java.awt.Label();
        label6 = new java.awt.Label();
        lblSexo = new javax.swing.JLabel();
        rdoMasc = new javax.swing.JRadioButton();
        rdoFem = new javax.swing.JRadioButton();
        txtRg = new javax.swing.JFormattedTextField();
        lblRg = new javax.swing.JLabel();
        lblCpf = new javax.swing.JLabel();
        lblLogo1 = new javax.swing.JLabel();
        lblTitulo1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblVendedor = new javax.swing.JTable();
        lblNome = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        lblEmail = new javax.swing.JLabel();
        btnEditar = new javax.swing.JButton();
        txtEmail = new javax.swing.JTextField();
        btnNovo = new javax.swing.JButton();
        lblObservacao = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtObservacao = new javax.swing.JTextArea();
        label7 = new java.awt.Label();
        lblDataNasc = new javax.swing.JLabel();
        btnSalvarContinuar = new javax.swing.JButton();
        label18 = new java.awt.Label();
        label5 = new java.awt.Label();
        txtDataNasc = new javax.swing.JFormattedTextField();
        txtCpf = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        lblNumeroTelefone = new javax.swing.JLabel();
        txtNumeroTelefone = new javax.swing.JFormattedTextField();
        lblOperadora = new javax.swing.JLabel();
        txtOperadora = new javax.swing.JTextField();
        lblTipo = new javax.swing.JLabel();
        cbxTipoTelefone = new javax.swing.JComboBox();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblVendedorTelefone = new javax.swing.JTable();
        label9 = new java.awt.Label();
        btnNovoTelefone = new javax.swing.JButton();
        btnExcluirTelefone = new javax.swing.JButton();
        btnEditarTelefone = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        lblTitulo2 = new javax.swing.JLabel();
        lblCEP = new javax.swing.JLabel();
        txtCEP = new javax.swing.JFormattedTextField();
        lblNumero = new javax.swing.JLabel();
        txtNumero = new javax.swing.JTextField();
        lblBairro = new javax.swing.JLabel();
        txtBairro = new javax.swing.JTextField();
        lblObservacaoFornecedorEnd = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        label10 = new java.awt.Label();
        label11 = new java.awt.Label();
        label12 = new java.awt.Label();
        lblComplemento = new javax.swing.JLabel();
        lblLogradouro = new javax.swing.JLabel();
        txtLogradouro = new javax.swing.JTextField();
        txtComplemento = new javax.swing.JTextField();
        lblCidade = new javax.swing.JLabel();
        txtCidade = new javax.swing.JTextField();
        label13 = new java.awt.Label();
        jLabel26 = new javax.swing.JLabel();
        cbxUF = new javax.swing.JComboBox();
        label15 = new java.awt.Label();
        label14 = new java.awt.Label();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblVendedorEndereco = new javax.swing.JTable();
        label16 = new java.awt.Label();
        btnNovoEndereco = new javax.swing.JButton();
        btnExcluirEndereco = new javax.swing.JButton();
        btnEditarEndereco = new javax.swing.JButton();
        btnSalvarFinalizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        label4.setForeground(new java.awt.Color(204, 0, 0));
        label4.setText("*");
        jPanel1.add(label4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 200, -1, -1));

        label6.setForeground(new java.awt.Color(204, 0, 0));
        label6.setText("*");
        jPanel1.add(label6, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 240, -1, -1));

        lblSexo.setText("Sexo");
        jPanel1.add(lblSexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 286, -1, -1));

        buttonGroup1.add(rdoMasc);
        rdoMasc.setText("Masculino");
        jPanel1.add(rdoMasc, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 282, -1, -1));

        buttonGroup1.add(rdoFem);
        rdoFem.setText("Feminino");
        jPanel1.add(rdoFem, new org.netbeans.lib.awtextra.AbsoluteConstraints(229, 282, -1, -1));
        jPanel1.add(txtRg, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 323, 310, -1));

        lblRg.setText("RG");
        jPanel1.add(lblRg, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 329, -1, -1));

        lblCpf.setText("CPF");
        jPanel1.add(lblCpf, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 374, -1, -1));

        lblLogo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N
        jPanel1.add(lblLogo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        lblTitulo1.setFont(new java.awt.Font("Arial Unicode MS", 0, 24)); // NOI18N
        lblTitulo1.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo1.setText("Lista de Funcionarios");
        jPanel1.add(lblTitulo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(228, 30, -1, 62));

        tblVendedor.setAutoCreateColumnsFromModel(false);
        tblVendedor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblVendedor.setName("tabelaCadastroFornecedor"); // NOI18N
        jScrollPane4.setViewportView(tblVendedor);

        jPanel1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 370, 480, 214));

        lblNome.setFont(new java.awt.Font("Arial Unicode MS", 0, 12)); // NOI18N
        lblNome.setText("Nome:");
        lblNome.setName("lblNome"); // NOI18N
        jPanel1.add(lblNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 156, -1, -1));

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Add User-100.png"))); // NOI18N
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 30, -1, -1));

        txtNome.setName("txtNome"); // NOI18N
        jPanel1.add(txtNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 155, 401, -1));

        lblEmail.setText("Email:");
        lblEmail.setName("lblEmail"); // NOI18N
        jPanel1.add(lblEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 205, -1, -1));

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setName("btnEditar"); // NOI18N
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        jPanel1.add(btnEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 600, -1, -1));

        txtEmail.setName("txtCNPJ"); // NOI18N
        txtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailActionPerformed(evt);
            }
        });
        jPanel1.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 200, 310, -1));

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.setActionCommand("Novo");
        btnNovo.setName("btnNovo"); // NOI18N
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });
        jPanel1.add(btnNovo, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 600, -1, -1));

        lblObservacao.setFont(new java.awt.Font("Arial Unicode MS", 0, 11)); // NOI18N
        lblObservacao.setText("Observação:");
        lblObservacao.setName("lblObservacao"); // NOI18N
        jPanel1.add(lblObservacao, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 420, 96, -1));

        txtObservacao.setColumns(20);
        txtObservacao.setRows(5);
        txtObservacao.setName("txtObservacao"); // NOI18N
        jScrollPane6.setViewportView(txtObservacao);

        jPanel1.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 420, 310, 138));

        label7.setForeground(new java.awt.Color(204, 0, 0));
        label7.setText("*");
        jPanel1.add(label7, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 370, -1, -1));

        lblDataNasc.setText("Data Nascimento");
        jPanel1.add(lblDataNasc, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 247, -1, -1));

        btnSalvarContinuar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Checkmark-26.png"))); // NOI18N
        btnSalvarContinuar.setText("Salvar e Continuar");
        btnSalvarContinuar.setName("btnSalvar"); // NOI18N
        btnSalvarContinuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarContinuarActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalvarContinuar, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 600, -1, -1));

        label18.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        label18.setForeground(new java.awt.Color(204, 0, 0));
        label18.setText("* Campos Obrigatórios");
        jPanel1.add(label18, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 603, -1, -1));

        label5.setForeground(new java.awt.Color(204, 0, 0));
        label5.setText("*");
        jPanel1.add(label5, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 150, -1, -1));

        try {
            txtDataNasc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtDataNasc, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 240, 310, -1));

        try {
            txtCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtCpf, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 370, 310, -1));

        tabPrincipal.addTab("Cadastro de Funcionarios", jPanel1);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N

        lblTitulo.setFont(new java.awt.Font("Arial Unicode MS", 0, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo.setText("Cadastro de Funcionario - Telefone");

        lblNumeroTelefone.setText("Número do telefone:");

        try {
            txtNumeroTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtNumeroTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroTelefoneActionPerformed(evt);
            }
        });

        lblOperadora.setText("Operadora:");

        txtOperadora.setName("txtOperadora"); // NOI18N

        lblTipo.setText("Tipo:");

        cbxTipoTelefone.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione...", "Residencial", "Comercial", "Celular", "Recado" }));
        cbxTipoTelefone.setName("comboItem"); // NOI18N
        cbxTipoTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxTipoTelefoneActionPerformed(evt);
            }
        });

        tblVendedorTelefone.setAutoCreateColumnsFromModel(false);
        tblVendedorTelefone.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblVendedorTelefone.setName("tabelaCadastroFornecedor"); // NOI18N
        jScrollPane7.setViewportView(tblVendedorTelefone);

        label9.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        label9.setForeground(new java.awt.Color(204, 0, 0));
        label9.setText("* Campos Obrigatórios");

        btnNovoTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnNovoTelefone.setText("Novo");
        btnNovoTelefone.setName("btnEditar"); // NOI18N
        btnNovoTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoTelefoneActionPerformed(evt);
            }
        });

        btnExcluirTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnExcluirTelefone.setText("Excluir");
        btnExcluirTelefone.setName("btnEditar"); // NOI18N
        btnExcluirTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirTelefoneActionPerformed(evt);
            }
        });

        btnEditarTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditarTelefone.setText("Editar");
        btnEditarTelefone.setName("btnEditar"); // NOI18N
        btnEditarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarTelefoneActionPerformed(evt);
            }
        });

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Checkmark-26.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.setName("btnSalvar"); // NOI18N
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(lblTitulo))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblNumeroTelefone)
                                .addGap(4, 4, 4)
                                .addComponent(txtNumeroTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblOperadora)
                                .addGap(47, 47, 47)
                                .addComponent(txtOperadora, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblTipo)
                                .addGap(79, 79, 79)
                                .addComponent(cbxTipoTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 891, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnNovoTelefone)
                                .addGap(18, 18, 18)
                                .addComponent(btnExcluirTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnEditarTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(btnSalvar)))))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(41, 41, 41)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblNumeroTelefone))
                    .addComponent(txtNumeroTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblOperadora))
                    .addComponent(txtOperadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblTipo))
                    .addComponent(cbxTipoTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditarTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnExcluirTelefone)
                        .addComponent(btnNovoTelefone)))
                .addContainerGap(37, Short.MAX_VALUE))
        );

        tabPrincipal.addTab("Cadastro de Funcionario - Telefone", jPanel2);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N

        lblTitulo2.setFont(new java.awt.Font("Arial Unicode MS", 0, 24)); // NOI18N
        lblTitulo2.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo2.setText("Cadastro de Funcionario - Endereço");

        lblCEP.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblCEP.setText("CEP:");
        lblCEP.setName("lblCEP"); // NOI18N

        try {
            txtCEP.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCEP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCEPActionPerformed(evt);
            }
        });

        lblNumero.setText("Número:");
        lblNumero.setName("lblNumero"); // NOI18N

        txtNumero.setName("txtNumero"); // NOI18N
        txtNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroActionPerformed(evt);
            }
        });

        lblBairro.setText("Bairro:");
        lblBairro.setName("lblBairro"); // NOI18N

        txtBairro.setEnabled(false);
        txtBairro.setName("txtBairro"); // NOI18N
        txtBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBairroActionPerformed(evt);
            }
        });

        lblObservacaoFornecedorEnd.setText("Observação:");
        lblObservacaoFornecedorEnd.setName("lblObservacao"); // NOI18N

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/search.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        label10.setForeground(new java.awt.Color(204, 0, 0));
        label10.setText("*");

        label11.setForeground(new java.awt.Color(204, 0, 0));
        label11.setText("*");

        label12.setForeground(new java.awt.Color(204, 0, 0));
        label12.setText("*");

        lblComplemento.setText("Complemento:");

        lblLogradouro.setText("Logradouro:");

        txtLogradouro.setEnabled(false);
        txtLogradouro.setName("txtLogadouro"); // NOI18N

        txtComplemento.setName("txtComplemento"); // NOI18N

        lblCidade.setText("Cidade:");

        txtCidade.setEnabled(false);
        txtCidade.setName("txtCidade"); // NOI18N

        label13.setForeground(new java.awt.Color(204, 0, 0));
        label13.setText("*");

        jLabel26.setText("UF:");

        cbxUF.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione..", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PR", "PB", "PA", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SE", "SP", "TO" }));
        cbxUF.setEnabled(false);
        cbxUF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxUFActionPerformed(evt);
            }
        });

        label15.setForeground(new java.awt.Color(204, 0, 0));
        label15.setText("*");

        label14.setForeground(new java.awt.Color(204, 0, 0));
        label14.setText("*");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setName("txtObservacao"); // NOI18N
        jScrollPane2.setViewportView(jTextArea1);

        tblVendedorEndereco.setAutoCreateColumnsFromModel(false);
        tblVendedorEndereco.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblVendedorEndereco.setName("tabelaCadastroFornecedor"); // NOI18N
        jScrollPane8.setViewportView(tblVendedorEndereco);

        label16.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        label16.setForeground(new java.awt.Color(204, 0, 0));
        label16.setText("* Campos Obrigatórios");

        btnNovoEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnNovoEndereco.setText("Novo");
        btnNovoEndereco.setName("btnEditar"); // NOI18N
        btnNovoEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoEnderecoActionPerformed(evt);
            }
        });

        btnExcluirEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnExcluirEndereco.setText("Excluir");
        btnExcluirEndereco.setName("btnEditar"); // NOI18N
        btnExcluirEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirEnderecoActionPerformed(evt);
            }
        });

        btnEditarEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditarEndereco.setText("Editar");
        btnEditarEndereco.setName("btnEditar"); // NOI18N
        btnEditarEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarEnderecoActionPerformed(evt);
            }
        });

        btnSalvarFinalizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Checkmark-26.png"))); // NOI18N
        btnSalvarFinalizar.setActionCommand("Salvar e Finalizar");
        btnSalvarFinalizar.setLabel("Salvar e Finalizar");
        btnSalvarFinalizar.setName("btnSalvar"); // NOI18N
        btnSalvarFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarFinalizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(330, 330, 330)
                        .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(181, 181, 181)
                        .addComponent(lblCidade)
                        .addGap(13, 13, 13)
                        .addComponent(txtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(label13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(jLabel26)
                        .addGap(3, 3, 3)
                        .addComponent(cbxUF, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(label14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(label16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnNovoEndereco)
                                .addGap(18, 18, 18)
                                .addComponent(btnExcluirEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnEditarEndereco)
                                .addGap(18, 18, 18)
                                .addComponent(btnSalvarFinalizar))
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(lblObservacaoFornecedorEnd))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(lblTitulo2))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(62, 62, 62)
                                .addComponent(lblCEP)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(btnBuscar))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(lblBairro)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(lblNumero)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(10, 10, 10)
                                .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(55, 55, 55)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblLogradouro)
                                .addGap(11, 11, 11)
                                .addComponent(txtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblComplemento)
                                .addGap(11, 11, 11)
                                .addComponent(txtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10)
                        .addComponent(label15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(28, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(lblTitulo2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(45, 45, 45)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCEP))
                    .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblLogradouro))
                    .addComponent(txtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblNumero))
                    .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblComplemento)
                    .addComponent(txtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblBairro))
                    .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCidade)
                    .addComponent(txtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(cbxUF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lblObservacaoFornecedorEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalvarFinalizar)
                        .addComponent(btnEditarEndereco)
                        .addComponent(btnNovoEndereco)
                        .addComponent(btnExcluirEndereco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(label16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45))
        );

        tabPrincipal.addTab("Cadastro de Funcionario - Endereço", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 983, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(tabPrincipal)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if(tblVendedor.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Selecione um Vendedor", "Alerta", 2);
	} else {
            Vendedor v = (Vendedor) tblVendedor.getValueAt(tblVendedor.getSelectedRow(), 0);
            atribuirVendedor(v);
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void txtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailActionPerformed

    private void btnSalvarContinuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarContinuarActionPerformed
        
        if (!validar()) {
            
        } else {
             obterVendedor();
            String erro = vendedorData.saveVendedor(vendedor);
            if (erro != null && !erro.isEmpty()) {
                JOptionPane.showMessageDialog(null, erro);
                return;
            }
    
            tabPrincipal.setEnabledAt(1,true);
            tabPrincipal.setEnabledAt(2,true);
            tabPrincipal.setSelectedIndex(1);
            atualizarTabela();
            atualizarTabelaEndereco();
            atualizarTabelaTelefone();
        }
        
    }//GEN-LAST:event_btnSalvarContinuarActionPerformed

    private void txtNumeroTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroTelefoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroTelefoneActionPerformed

    private void cbxTipoTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxTipoTelefoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxTipoTelefoneActionPerformed

    private void btnNovoTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoTelefoneActionPerformed
        limparTelefone();
        idTelefone = 0;
    }//GEN-LAST:event_btnNovoTelefoneActionPerformed

    private void btnExcluirTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirTelefoneActionPerformed
        if (tblVendedorTelefone.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Selecione um Telefone", "Alerta", 2);
        } else {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja excluir?", "Atenção!", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                Object selectedObject = (Object) tblVendedorTelefone.getModel().getValueAt(tblVendedorTelefone.getSelectedRow(), 0);
                Telefone tel = Telefone.class.cast(selectedObject);

                telefoneData.deleteTelefone(tel);
                atualizarTabelaTelefone();
                limparTelefone();
            }
        }

    }//GEN-LAST:event_btnExcluirTelefoneActionPerformed

    private void btnEditarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarTelefoneActionPerformed
       
        //Object selectedObject = (Object) tblVendedorTelefone.getModel().getValueAt(tblVendedorTelefone.getSelectedRow(), 0);
        //Object selectedObjectTipoTelefone = (Object) tblVendedorTelefone.getModel().getValueAt(tblVendedorTelefone.getSelectedRow(), 1);
        //Telefone tel = Telefone.class.cast(selectedObject);
        //Tipotelefone tipoTel = Tipotelefone.class.cast(selectedObjectTipoTelefone);
        
        telefone = (Telefone) tblVendedorTelefone.getModel().getValueAt(tblVendedorTelefone.getSelectedRow(), 0);
        idTelefone = telefone.getIdTelefone();
        
        Tipotelefone item = tipoTelefoneData.getTipotelefone(telefone.getTipotelefone().getIdTipoTelefone());
        
        cbxTipoTelefone.setSelectedItem(item);
        
        atribuirTelefone(telefone);
        atualizarTabelaTelefone();

    }//GEN-LAST:event_btnEditarTelefoneActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if (validarTelefone()) {
            Telefone telefone = obterTelefone();
            salvarTelefone(telefone);

        }

        atualizarTabelaTelefone();
        limparTelefone();

        //idTelefone = null;

        tabPrincipal.setEnabledAt(2,true);
        atualizarTabelaEndereco();

    }//GEN-LAST:event_btnSalvarActionPerformed

    private void txtCEPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCEPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCEPActionPerformed

    private void txtNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroActionPerformed

    private void txtBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBairroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBairroActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        endereco = enderecoData.getEnderecoCep(txtCEP.getText());
        txtLogradouro.setText(endereco.getEnderecoCompleto());
        txtBairro.setText(endereco.getBairro().getBairro());
        txtCidade.setText(endereco.getBairro().getCidade().getCidade());
        //txtNumero.setText(1+"");
        cbxUF.addItem(endereco.getBairro().getCidade().getEstado().getUf());
        cbxUF.setSelectedItem(endereco.getBairro().getCidade().getEstado().getUf());

    }//GEN-LAST:event_btnBuscarActionPerformed

    private void cbxUFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxUFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxUFActionPerformed

    private void btnNovoEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoEnderecoActionPerformed
        limparEndereco();
        idEndereco = 0;
    }//GEN-LAST:event_btnNovoEnderecoActionPerformed

    private void btnExcluirEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirEnderecoActionPerformed
        if (tblVendedorEndereco.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Selecione um Endereço", "Alerta", 2);
        } else {
            Object selectedObject = (Object) tblVendedorEndereco.getModel().getValueAt(tblVendedorEndereco.getSelectedRow(), 0);
            Enderecopessoa end = Enderecopessoa.class.cast(selectedObject);

            enderecopessoaData.deleteEnderecoPessoa(end);
            atualizarTabelaEndereco();
            limparEndereco();
        }
    }//GEN-LAST:event_btnExcluirEnderecoActionPerformed

    private void btnEditarEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarEnderecoActionPerformed
        if(tblVendedorEndereco.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Selecione um Endereço", "Alerta", 2);
        } else {
            Enderecopessoa ep = (Enderecopessoa) tblVendedorEndereco.getValueAt(tblVendedorEndereco.getSelectedRow(), 0);
            endereco = enderecoData.getEndereco(ep.getId().getIdEndereco());
            atribuirEndereco(ep);
        }
    }//GEN-LAST:event_btnEditarEnderecoActionPerformed

    private void btnSalvarFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarFinalizarActionPerformed
        if (validarEndereco()) {
            obterEndereco();//obtem
            enderecopessoaData.insertUpdate(enderecoPessoa);//salva
        }

        atualizarTabelaEndereco();
        limparEndereco();
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);

    }//GEN-LAST:event_btnSalvarFinalizarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limparFuncionario();
        limparTelefone();
        limparEndereco();
        tabPrincipal.setEnabledAt(1,false);
        tabPrincipal.setEnabledAt(2,false);
    }//GEN-LAST:event_btnNovoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormFuncionario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEditarEndereco;
    private javax.swing.JButton btnEditarTelefone;
    private javax.swing.JButton btnExcluirEndereco;
    private javax.swing.JButton btnExcluirTelefone;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnNovoEndereco;
    private javax.swing.JButton btnNovoTelefone;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnSalvarContinuar;
    private javax.swing.JButton btnSalvarFinalizar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbxTipoTelefone;
    private javax.swing.JComboBox cbxUF;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTextArea jTextArea1;
    private java.awt.Label label10;
    private java.awt.Label label11;
    private java.awt.Label label12;
    private java.awt.Label label13;
    private java.awt.Label label14;
    private java.awt.Label label15;
    private java.awt.Label label16;
    private java.awt.Label label18;
    private java.awt.Label label4;
    private java.awt.Label label5;
    private java.awt.Label label6;
    private java.awt.Label label7;
    private java.awt.Label label9;
    private javax.swing.JLabel lblBairro;
    private javax.swing.JLabel lblCEP;
    private javax.swing.JLabel lblCidade;
    private javax.swing.JLabel lblComplemento;
    private javax.swing.JLabel lblCpf;
    private javax.swing.JLabel lblDataNasc;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblLogo1;
    private javax.swing.JLabel lblLogradouro;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNumero;
    private javax.swing.JLabel lblNumeroTelefone;
    private javax.swing.JLabel lblObservacao;
    private javax.swing.JLabel lblObservacaoFornecedorEnd;
    private javax.swing.JLabel lblOperadora;
    private javax.swing.JLabel lblRg;
    private javax.swing.JLabel lblSexo;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblTitulo1;
    private javax.swing.JLabel lblTitulo2;
    private javax.swing.JRadioButton rdoFem;
    private javax.swing.JRadioButton rdoMasc;
    private javax.swing.JTabbedPane tabPrincipal;
    private javax.swing.JTable tblVendedor;
    private javax.swing.JTable tblVendedorEndereco;
    private javax.swing.JTable tblVendedorTelefone;
    private javax.swing.JTextField txtBairro;
    private javax.swing.JFormattedTextField txtCEP;
    private javax.swing.JTextField txtCidade;
    private javax.swing.JTextField txtComplemento;
    private javax.swing.JFormattedTextField txtCpf;
    private javax.swing.JFormattedTextField txtDataNasc;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtLogradouro;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtNumero;
    private javax.swing.JFormattedTextField txtNumeroTelefone;
    private javax.swing.JTextArea txtObservacao;
    private javax.swing.JTextField txtOperadora;
    private javax.swing.JFormattedTextField txtRg;
    // End of variables declaration//GEN-END:variables
}
