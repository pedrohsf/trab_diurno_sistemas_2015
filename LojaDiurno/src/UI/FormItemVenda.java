package UI;

import Data.ItemVendaData;
import Data.MovimentacaoData;
import Data.ProdutoData;
import business.Itemvenda;
import business.ItemvendaId;
import business.Movimentacao;
import business.Produto;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author AndersonLuis
 */
public class FormItemVenda extends javax.swing.JFrame
{
    ProdutoData produtoData;
    MovimentacaoData movimentacaoData;
    ItemVendaData itemVendaData;
    Produto produto;
    Itemvenda itemVenda;
    TelaVenda telaVenda;
    
    public FormItemVenda()
    {
        initComponents();
        
        this.setLocationRelativeTo(null);
        
        produtoData = new ProdutoData();
        movimentacaoData = new MovimentacaoData();
        itemVendaData = new ItemVendaData();
        produto = new Produto();
        itemVenda = new Itemvenda();
    }
            
    public FormItemVenda(TelaVenda telaVenda)
    {
        initComponents();
        
        this.setLocationRelativeTo(null);
        
        produtoData = new ProdutoData();
        movimentacaoData = new MovimentacaoData();
        produto = new Produto();
        itemVenda = new Itemvenda();
        
        this.telaVenda = telaVenda;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtNomeProduto = new javax.swing.JTextField();
        lblNomeProduto = new javax.swing.JLabel();
        jsItemVenda = new javax.swing.JScrollPane();
        tabItemVenda = new javax.swing.JTable();
        cntQuantidade = new javax.swing.JSpinner();
        lblQuantidade = new javax.swing.JLabel();
        lblProduto = new javax.swing.JLabel();
        lblProdutoDescricao = new javax.swing.JLabel();
        lblValorTotal = new javax.swing.JLabel();
        btnAdicionarItemVenda = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lblValorTotalDescricao = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Adicionar Item Venda");
        setName("frmItemVenda"); // NOI18N
        setResizable(false);

        txtNomeProduto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNomeProdutoKeyPressed(evt);
            }
        });

        lblNomeProduto.setText("Nome do Produto");

        tabItemVenda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Produto", "Valor Unitário"
            }
        ));
        tabItemVenda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabItemVendaMouseClicked(evt);
            }
        });
        jsItemVenda.setViewportView(tabItemVenda);

        cntQuantidade.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(0), null, Integer.valueOf(1)));
        cntQuantidade.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cntQuantidadeStateChanged(evt);
            }
        });

        lblQuantidade.setText("Quantidade");

        lblProduto.setText("Produto");

        lblProdutoDescricao.setText(" ");

        lblValorTotal.setText("Valor Total");

        btnAdicionarItemVenda.setText("Adicionar Item Venda");
        btnAdicionarItemVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarItemVendaActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        lblValorTotalDescricao.setText(" ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblProduto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblProdutoDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAdicionarItemVenda))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblNomeProduto)
                        .addComponent(jsItemVenda, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                        .addComponent(txtNomeProduto))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblValorTotal)
                            .addComponent(lblQuantidade))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cntQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblValorTotalDescricao, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(lblNomeProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jsItemVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProduto)
                    .addComponent(lblProdutoDescricao))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cntQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblQuantidade))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblValorTotal)
                    .addComponent(lblValorTotalDescricao))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAdicionarItemVenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNomeProdutoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeProdutoKeyPressed
        if(txtNomeProduto.getText().length() >= 3)
        {
            AtualizarTabela(listarProdutosPorDescricao());
        }
    }//GEN-LAST:event_txtNomeProdutoKeyPressed

    private void btnAdicionarItemVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarItemVendaActionPerformed
        int quantidadeInformada = (int) (cntQuantidade.getValue());
        
        if(quantidadeInformada <= 0)
        {
            JOptionPane.showMessageDialog(null, "Informe a quantidade desejada para o Produto.");
        }
        
        else
        {
            telaVenda.adicionarItemVenda(this.itemVenda);
            fecharFormulario();
        }
    }//GEN-LAST:event_btnAdicionarItemVendaActionPerformed

    private void tabItemVendaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabItemVendaMouseClicked
        Object selectedObject = (Object) tabItemVenda.getModel().getValueAt(tabItemVenda.getSelectedRow(), 0);
        Produto produtoTabela = Produto.class.cast(selectedObject);
        this.produto = produtoTabela;
        itemVenda.setProduto(produtoTabela);
        lblProdutoDescricao.setText(produto.getDescricao());
    }//GEN-LAST:event_tabItemVendaMouseClicked
    
    private void fecharFormulario()
    {
        this.dispose();
    }
    
    private void cntQuantidadeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_cntQuantidadeStateChanged
        int quantidadeInformada = (int) (cntQuantidade.getValue());
        
        BigDecimal quantidadeEstoque = calcularEstoque(produto.getIdProduto());
        
        if(possivelVender(quantidadeInformada,quantidadeEstoque))
        {
            BigDecimal valorTotal = calcularValorTotal(quantidadeInformada, produto.getValorVenda());
            itemVenda.setValorItem(valorTotal);
            itemVenda.setQuantidade(new BigDecimal(quantidadeInformada));
            lblValorTotalDescricao.setText(valorTotal.toString());
        }
        
        else if(quantidadeInformada != 0)
        {
            JOptionPane.showMessageDialog(null,"Quantidade não suficiente para a Venda");
            cntQuantidade.setValue(quantidadeInformada - 1);
        }
    }//GEN-LAST:event_cntQuantidadeStateChanged

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        fecharFormulario();
    }//GEN-LAST:event_btnCancelarActionPerformed
    
    private boolean possivelVender(int quantidadeInformada, BigDecimal quantidadeEstoque)
    {
        //Quantidade no Estoque é menor que quantidade informada
        if(quantidadeEstoque.compareTo(new BigDecimal(quantidadeInformada)) == -1)
        {
            return false;
        }
        
        //Quantidade no Estoque é menor que 0
        if(quantidadeEstoque.compareTo(new BigDecimal(0)) == -1)
        {
            return false;
        }
        
        //Quantidade no Estoque é 0
        if(quantidadeEstoque.compareTo(new BigDecimal(0)) == 0)
        {
            return false;
        }
        
        return true;
    }
    
    private BigDecimal calcularEstoque(Integer idProduto)
    {
        List<Movimentacao> movEntrada;
        List<Movimentacao> movSaida;
        
        movEntrada = movimentacaoData.selectMovimentacaoEntrada(produto.getIdProduto());
        movSaida = movimentacaoData.selectMovimentacaoSaida(produto.getIdProduto());
        
        BigDecimal quantidade = new BigDecimal(0);
        
        for(Movimentacao m : movEntrada)
        {
            quantidade = quantidade.add(m.getQuantidade());
        }
        
        for(Movimentacao m : movSaida)
        {
            quantidade = quantidade.subtract(m.getQuantidade());
        }
        
        return quantidade;
    }
    
    private BigDecimal calcularValorTotal(int quantidade, BigDecimal preco)
    {
        BigDecimal itemCost  = BigDecimal.ZERO;
        BigDecimal totalCost = BigDecimal.ZERO;
        
        itemCost  = preco.multiply(new BigDecimal(quantidade));
        totalCost = totalCost.add(itemCost);
        return totalCost;
    }
    
    private List<Produto> listarProdutosPorDescricao()
    {
        String Descricao = txtNomeProduto.getText();
        List<Produto> produtos = produtoData.selectProduto(Descricao);
        return produtos;
    }
    
    private void AtualizarTabela(List<Produto> produtos)
    {
        tabItemVenda = new JTable();
        
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Produto", "Valor Unitário" }, 0)
        {
            boolean[] canEdit = new boolean[]
            {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };
        
        model.setRowCount(0);
        
        for (Produto p : produtos) {
            
            model.addRow(new Object[]{ 
                p,
                p.getDescricao(),
                p.getValorVenda()
            });
        }
        
        tabItemVenda.setModel(model);
        
        tabItemVenda.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    Object selectedObject = (Object) tabItemVenda.getModel().getValueAt(tabItemVenda.getSelectedRow(), 0);
                    Produto produtoTabela = Produto.class.cast(selectedObject);
                    produto = produtoTabela;
                    itemVenda.setProduto(produtoTabela);
                    lblProdutoDescricao.setText(produto.getDescricao());
                }
            }
        });
        
        tabItemVenda.setAutoCreateRowSorter(true);
        tabItemVenda.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tabItemVenda.getColumnModel().getColumn(0).setMinWidth(0);
        tabItemVenda.getColumnModel().getColumn(0).setPreferredWidth(0);
        tabItemVenda.getColumnModel().getColumn(1).setPreferredWidth(250);
        tabItemVenda.getColumnModel().getColumn(2).setPreferredWidth(150);
        tabItemVenda.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jsItemVenda.setViewportView(tabItemVenda);
    }
    
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormItemVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormItemVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormItemVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormItemVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormItemVenda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionarItemVenda;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JSpinner cntQuantidade;
    private javax.swing.JScrollPane jsItemVenda;
    private javax.swing.JLabel lblNomeProduto;
    private javax.swing.JLabel lblProduto;
    private javax.swing.JLabel lblProdutoDescricao;
    private javax.swing.JLabel lblQuantidade;
    private javax.swing.JLabel lblValorTotal;
    private javax.swing.JLabel lblValorTotalDescricao;
    private javax.swing.JTable tabItemVenda;
    private javax.swing.JTextField txtNomeProduto;
    // End of variables declaration//GEN-END:variables
}
