
package UI;
import Data.FotoData;
import Data.MarcaData;
import Data.ProdutoData;
import business.Foto;
import business.Marca;
import business.Produto;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Leandro
 */
public class FormProduto extends javax.swing.JFrame 
{
    private int idProduto;
    private Short idFoto;
    private ProdutoData produtoData;
    private MarcaData marcaData;
    private Produto produto;
    private Marca marca;
    private Foto foto;
    private FotoData fotoData;
    private boolean saveProduto=true;
    private boolean saveFoto=true;

    public FormProduto() 
    {
        initComponents();
        marcaData =  new MarcaData();
        produtoData =  new ProdutoData();
        fotoData = new FotoData();
        limparFormulario();
        atualizarTabela(produtoData.selectProduto());
        inserirMarcaCbx();
        inserirProdutoCbx();
        setIconeQuadroDeFoto();
    }
    private void inserirMarcaCbx()
    {
         
         cbxMarca.removeAllItems();
        for(Object m : marcaData.selectMarca())
        {
      
            cbxMarca.addItem(m);
        }
        Produto p = new Produto();
        p.setMarca((Marca) cbxMarca.getSelectedItem());
        
    }
    private void limparFormulario()
    {
        idProduto=0;
        txtDescricao.setText("");
        txtModelo.setText("");
        txtCodigoBarras.setText("");
        txtPeso.setText("");
        txtValorVenda.setText("");
        txtValorCompra.setText("");
        txtLucro.setText("");
        txtQtde.setText("");
        txtAltura.setText("");
        txtLargura.setText("");
        txtComprimento.setText("");
        cbxMarca.setSelectedIndex(-1);
        produto =  new Produto();
    }
    
    
    private void atribuirProduto(Produto p)
    {
        idProduto =  p.getIdProduto();
        txtDescricao.setText(p.getDescricao());
        txtModelo.setText(p.getModelo());
        cbxMarca.setSelectedIndex(p.getMarca().getIdMarca());
        txtCodigoBarras.setText(p.getCodigoBarras());
        txtPeso.setText(""+p.getPeso());
        txtValorVenda.setText(""+p.getValorVenda());
        txtValorCompra.setText(""+p.getValorCompra());
        txtLucro.setText(""+p.getPercentualLucro());
        txtQtde.setText(""+p.getQuantidadeInicial());
        txtAltura.setText(""+p.getAltura());
        txtLargura.setText(""+p.getLargura());
        txtComprimento.setText(""+p.getComprimento());
       
    }
    private void obterProduto()
    {
        float val=0;
       produto = new Produto();
       produto.setIdProduto(idProduto);
       produto.setDescricao(txtDescricao.getText());
       produto.setModelo(txtModelo.getText());
       produto.setMarca((Marca)cbxMarca.getSelectedItem());
       produto.setCodigoBarras(txtCodigoBarras.getText());   
       
       
       val =  Float.parseFloat(txtPeso.getText());
       produto.setPeso(BigDecimal.valueOf(val));
       
       val=0;
       val =  Float.parseFloat(txtValorCompra.getText());
       produto.setValorCompra(BigDecimal.valueOf(val));
       
       val=0;
       val =  Float.parseFloat(txtValorVenda.getText());
       produto.setValorVenda(BigDecimal.valueOf(val));
       
       val=0;
       val =  Float.parseFloat(txtLucro.getText());
       produto.setPercentualLucro(BigDecimal.valueOf(val));
       
       
       val =  Float.parseFloat(txtQtde.getText());
       produto.setQuantidadeInicial(BigDecimal.valueOf(val));
       
       val=0;
       val =  Float.parseFloat(txtAltura.getText());
       produto.setAltura(BigDecimal.valueOf(val));
       
       val=0;
       val =  Float.parseFloat(txtLargura.getText());
       produto.setLargura(BigDecimal.valueOf(val));
       
       val=0;
       val =  Float.parseFloat(txtComprimento.getText());
       produto.setComprimento(BigDecimal.valueOf(val));
       
    }
     private boolean validar()
    {
        if(txtDescricao.getText().isEmpty() || txtDescricao.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtDescricao, "Insira a descrição do produto!","Alerta",2);
            return false;
        }
        if(txtModelo.getText().isEmpty() || txtModelo.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtModelo, "Insira o modelo do produto!","Alerta",2);
            return false;
        }
        if(cbxMarca.getSelectedItem()==null)
        {
            JOptionPane.showMessageDialog(cbxMarca, "Selecione a marca do produto!","Alerta",2);
            return false;
        }
        if(txtCodigoBarras.getText().isEmpty() || txtCodigoBarras.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtCodigoBarras, "Insira o codigo de barras do produto!","Alerta",2);
            return false;
        }
        if(txtPeso.getText().isEmpty() || txtPeso.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtPeso, "Insira o peso do produto!","Alerta",2);
            return false;
        }
        if(txtValorCompra.getText().isEmpty() || txtValorCompra.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtValorCompra, "Insira o valor de compra do produto!","Alerta",2);
            return false;
        }
        if(txtValorVenda.getText().isEmpty() || txtValorVenda.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtValorVenda, "Insira o valor de venda do produto!","Alerta",2);
            return false;
        }
        if(txtLucro.getText().isEmpty() || txtLucro.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtLucro, "Insira o percentual de lucro do produto!","Alerta",2);
            return false;
        }
        if(txtQtde.getText().isEmpty() || txtQtde.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtQtde, "Insira a quantidade inicial do produto!","Alerta",2);
            return false;
        }
        if(txtAltura.getText().isEmpty() || txtAltura.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtAltura, "Insira a altura do produto!","Alerta",2);
            return false;
        }
        if(txtLargura.getText().isEmpty() || txtLargura.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtLargura, "Insira a largura do produto!","Alerta",2);
            return false;
        }
        if(txtComprimento.getText().isEmpty() || txtComprimento.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtComprimento, "Insira o comprimento do produto!","Alerta",2);
            return false;
        }
        return true;
    }
    private void atualizarTabela(List<Produto> produtos)
    {
        tblProdutos = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Descrição", "Modelo","Marca", "Codigo de Barras",
        "Peso","Valor de Venda","Lucro%","Valor de Compra","Quantidade","Altura","Largura","Comprimento"}, 0)
        {
            private static final long serialVersionUID = 1L;
            boolean[] canEdit = new boolean[]
            {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) 
            {
                return canEdit[columnIndex];
            }
        };
 
        model.setRowCount(0);
       
        for (Produto p : produtos) 
        {
            
            model.addRow(new Object[]
            { 
                p, 
                p.getDescricao(),
                p.getModelo(),
                p.getMarca().getDescricao(),
                p.getCodigoBarras(),
                p.getPeso(),
                p.getValorVenda(),
                p.getPercentualLucro(),
                p.getValorCompra(),
                p.getQuantidadeInicial(),
                p.getAltura(),
                p.getLargura(),
                p.getComprimento()
            });
        }
        tblProdutos.setModel(model);
       
        tblProdutos.addMouseListener(new MouseAdapter() 
        {
            public void mouseClicked(MouseEvent e) 
            {
                if (e.getClickCount() == 2) 
                {
                    btnEditarActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        tblProdutos.setAutoCreateRowSorter(true);
        tblProdutos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblProdutos.getColumnModel().getColumn(0).setMinWidth(0);
        tblProdutos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblProdutos.getColumnModel().getColumn(1).setPreferredWidth(200);
        tblProdutos.getColumnModel().getColumn(2).setPreferredWidth(150);
        tblProdutos.getColumnModel().getColumn(3).setPreferredWidth(180);
        tblProdutos.getColumnModel().getColumn(4).setPreferredWidth(150);
        tblProdutos.getColumnModel().getColumn(5).setPreferredWidth(115);
        tblProdutos.getColumnModel().getColumn(6).setPreferredWidth(115);
        tblProdutos.getColumnModel().getColumn(7).setPreferredWidth(115);
        tblProdutos.getColumnModel().getColumn(8).setPreferredWidth(115);
        tblProdutos.getColumnModel().getColumn(9).setPreferredWidth(115);
        tblProdutos.getColumnModel().getColumn(10).setPreferredWidth(115);
        tblProdutos.getColumnModel().getColumn(11).setPreferredWidth(115);
        tblProdutos.getColumnModel().getColumn(12).setPreferredWidth(115);
        tblProdutos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
       jScrollPane1.setViewportView(tblProdutos);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabProduto = new javax.swing.JTabbedPane();
        tabCadastroProduto = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblDescrição1 = new javax.swing.JLabel();
        txtDescricao = new javax.swing.JTextField();
        lblModelo1 = new javax.swing.JLabel();
        txtModelo = new javax.swing.JTextField();
        cbxMarca = new javax.swing.JComboBox();
        lblMarca = new javax.swing.JLabel();
        lblAltura = new javax.swing.JLabel();
        txtCodigoBarras = new javax.swing.JTextField();
        lblPeso1 = new javax.swing.JLabel();
        txtPeso = new javax.swing.JTextField();
        lblValorVenda2 = new javax.swing.JLabel();
        txtValorVenda = new javax.swing.JTextField();
        lblLucro = new javax.swing.JLabel();
        txtLucro = new javax.swing.JTextField();
        lblValorCompra = new javax.swing.JLabel();
        txtValorCompra = new javax.swing.JTextField();
        lblModelo3 = new javax.swing.JLabel();
        txtComprimento = new javax.swing.JTextField();
        lblLargura = new javax.swing.JLabel();
        lblPesquisa = new javax.swing.JLabel();
        txtAltura = new javax.swing.JTextField();
        txtQtde = new javax.swing.JTextField();
        lblLargura1 = new javax.swing.JLabel();
        txtLargura = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProdutos = new javax.swing.JTable();
        btnNovo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        lblQtde3 = new javax.swing.JLabel();
        lblIconePesquisa = new javax.swing.JLabel();
        btnHomeCadastro = new javax.swing.JButton();
        btnAlterarFoto = new javax.swing.JButton();
        lblLogo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        tabEstoqueProduto = new javax.swing.JPanel();
        lblTituloEstoque = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProdutosEstoque = new javax.swing.JTable();
        btnHomeEstoque = new javax.swing.JButton();
        lblLogo1 = new javax.swing.JLabel();
        tabCadastroFotoProduto = new javax.swing.JPanel();
        lblFoto = new javax.swing.JLabel();
        btnProcurar = new javax.swing.JButton();
        txtNomeFoto = new javax.swing.JTextField();
        cbxProduto = new javax.swing.JComboBox();
        lblProduto = new javax.swing.JLabel();
        lblTituloEstoque1 = new javax.swing.JLabel();
        txtCaminho = new javax.swing.JTextField();
        lblProduto1 = new javax.swing.JLabel();
        btnSalvarFotos = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnHomeFoto = new javax.swing.JButton();
        lblIconFoto = new javax.swing.JLabel();
        btnVerFotosCadastradas = new javax.swing.JButton();
        lblLogo2 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));
        setResizable(false);

        tabCadastroProduto.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo.setFont(new java.awt.Font("Arial Unicode MS", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo.setText("Cadastro de Produto");
        tabCadastroProduto.add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, 260, 62));

        lblDescrição1.setText("Descrição:");
        lblDescrição1.setName("lblNome"); // NOI18N
        tabCadastroProduto.add(lblDescrição1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 105, 80, -1));
        tabCadastroProduto.add(txtDescricao, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 100, 280, 25));

        lblModelo1.setText("Modelo:");
        lblModelo1.setName("lblModelo"); // NOI18N
        tabCadastroProduto.add(lblModelo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 105, 50, -1));
        tabCadastroProduto.add(txtModelo, new org.netbeans.lib.awtextra.AbsoluteConstraints(548, 100, 332, 25));

        tabCadastroProduto.add(cbxMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(943, 100, 330, 25));

        lblMarca.setText("Marca:");
        lblMarca.setName("lblMarca"); // NOI18N
        tabCadastroProduto.add(lblMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(884, 100, 40, 20));

        lblAltura.setText("Altura:");
        lblAltura.setName("lblModelo"); // NOI18N
        tabCadastroProduto.add(lblAltura, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 190, 40, 30));
        tabCadastroProduto.add(txtCodigoBarras, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 144, 280, 25));

        lblPeso1.setText("Peso:");
        lblPeso1.setName("lblNome"); // NOI18N
        tabCadastroProduto.add(lblPeso1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 150, 40, -1));
        tabCadastroProduto.add(txtPeso, new org.netbeans.lib.awtextra.AbsoluteConstraints(548, 144, 90, 25));

        lblValorVenda2.setText("Valor de venda:");
        lblValorVenda2.setName("lblNome"); // NOI18N
        tabCadastroProduto.add(lblValorVenda2, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 150, 90, -1));
        tabCadastroProduto.add(txtValorVenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 144, 100, 25));

        lblLucro.setText("Lucro %:");
        lblLucro.setName("lblNome"); // NOI18N
        tabCadastroProduto.add(lblLucro, new org.netbeans.lib.awtextra.AbsoluteConstraints(884, 149, 50, -1));

        txtLucro.setPreferredSize(new java.awt.Dimension(6, 24));
        tabCadastroProduto.add(txtLucro, new org.netbeans.lib.awtextra.AbsoluteConstraints(944, 144, 110, 25));

        lblValorCompra.setText("Valor de compra:");
        lblValorCompra.setName("lblNome"); // NOI18N
        tabCadastroProduto.add(lblValorCompra, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 150, 100, -1));
        tabCadastroProduto.add(txtValorCompra, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 140, 90, 25));

        lblModelo3.setText("Codigo de barras:");
        lblModelo3.setName("lblModelo"); // NOI18N
        tabCadastroProduto.add(lblModelo3, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 144, 100, 20));
        tabCadastroProduto.add(txtComprimento, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 194, 100, 25));

        lblLargura.setText("Comprimento:");
        lblLargura.setName("lblModelo"); // NOI18N
        tabCadastroProduto.add(lblLargura, new org.netbeans.lib.awtextra.AbsoluteConstraints(657, 200, 90, 20));

        lblPesquisa.setText("Digite a descrição do produto para pesquisar:");
        lblPesquisa.setName("lblModelo"); // NOI18N
        tabCadastroProduto.add(lblPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 260, 260, 30));
        tabCadastroProduto.add(txtAltura, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 194, 90, 25));
        tabCadastroProduto.add(txtQtde, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 194, 100, 25));

        lblLargura1.setText("Largura:");
        lblLargura1.setName("lblModelo"); // NOI18N
        tabCadastroProduto.add(lblLargura1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 190, 60, 30));
        tabCadastroProduto.add(txtLargura, new org.netbeans.lib.awtextra.AbsoluteConstraints(548, 194, 92, 25));

        tblProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblProdutos);

        tabCadastroProduto.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 294, 1250, 230));

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/IconeNovo.png"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.setToolTipText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });
        tabCadastroProduto.add(btnNovo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 534, 120, 35));

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setToolTipText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        tabCadastroProduto.add(btnEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 534, 120, 35));

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnExcluir.setText("Excluir");
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        tabCadastroProduto.add(btnExcluir, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 534, 120, 35));

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Checkmark-26.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.setToolTipText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        tabCadastroProduto.add(btnSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 534, 120, 35));

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyReleased(evt);
            }
        });
        tabCadastroProduto.add(txtPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 260, 940, 25));

        lblQtde3.setText("Quantidade:");
        lblQtde3.setName("lblModelo"); // NOI18N
        tabCadastroProduto.add(lblQtde3, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 196, 80, 20));

        lblIconePesquisa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/IconePesquisar.png"))); // NOI18N
        tabCadastroProduto.add(lblIconePesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 250, -1, 40));

        btnHomeCadastro.setBackground(new java.awt.Color(204, 0, 0));
        btnHomeCadastro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Iconehome.png"))); // NOI18N
        btnHomeCadastro.setToolTipText("Home");
        tabCadastroProduto.add(btnHomeCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(1240, 30, 40, 40));

        btnAlterarFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/IconeCamera.png"))); // NOI18N
        btnAlterarFoto.setText("Alterar Foto");
        btnAlterarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarFotoActionPerformed(evt);
            }
        });
        tabCadastroProduto.add(btnAlterarFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 534, 150, 35));

        lblLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N
        tabCadastroProduto.add(lblLogo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, -1, -1));

        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("*");
        tabCadastroProduto.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 200, 20, -1));

        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setText("*");
        tabCadastroProduto.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 140, 20, -1));

        jLabel3.setForeground(new java.awt.Color(255, 0, 0));
        jLabel3.setText("*");
        tabCadastroProduto.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 100, 20, -1));

        jLabel4.setForeground(new java.awt.Color(255, 0, 0));
        jLabel4.setText("*");
        tabCadastroProduto.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 254, 20, 20));

        jLabel5.setForeground(new java.awt.Color(255, 0, 0));
        jLabel5.setText("*");
        tabCadastroProduto.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 190, 20, -1));

        jLabel6.setForeground(new java.awt.Color(255, 0, 0));
        jLabel6.setText("*");
        tabCadastroProduto.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 100, 20, -1));

        jLabel7.setForeground(new java.awt.Color(255, 0, 0));
        jLabel7.setText("*");
        tabCadastroProduto.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 150, 20, -1));

        jLabel8.setForeground(new java.awt.Color(255, 0, 0));
        jLabel8.setText("*");
        tabCadastroProduto.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 140, 10, 20));

        jLabel9.setForeground(new java.awt.Color(255, 0, 0));
        jLabel9.setText("*");
        tabCadastroProduto.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 190, 20, 20));

        jLabel10.setForeground(new java.awt.Color(255, 0, 0));
        jLabel10.setText("*");
        tabCadastroProduto.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 140, 20, 20));

        jLabel11.setForeground(new java.awt.Color(255, 0, 0));
        jLabel11.setText("*");
        tabCadastroProduto.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 100, 10, 20));

        jLabel12.setForeground(new java.awt.Color(255, 0, 0));
        jLabel12.setText("*");
        tabCadastroProduto.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 140, 10, -1));

        jLabel13.setForeground(new java.awt.Color(255, 0, 0));
        jLabel13.setText("*");
        tabCadastroProduto.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, 20, 20));

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 0, 51));
        jLabel17.setText("* Campos Obrigatórios");
        tabCadastroProduto.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 580, -1, -1));

        tabProduto.addTab("Cadastro de Produto", tabCadastroProduto);

        tabEstoqueProduto.setLayout(null);

        lblTituloEstoque.setFont(new java.awt.Font("Arial Unicode MS", 1, 24)); // NOI18N
        lblTituloEstoque.setForeground(new java.awt.Color(102, 102, 102));
        lblTituloEstoque.setText("Estoque de Produto");
        tabEstoqueProduto.add(lblTituloEstoque);
        lblTituloEstoque.setBounds(240, 30, 260, 62);

        tblProdutosEstoque.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblProdutosEstoque);

        tabEstoqueProduto.add(jScrollPane2);
        jScrollPane2.setBounds(30, 300, 1250, 230);

        btnHomeEstoque.setBackground(new java.awt.Color(204, 0, 0));
        btnHomeEstoque.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Iconehome.png"))); // NOI18N
        btnHomeEstoque.setToolTipText("Home");
        tabEstoqueProduto.add(btnHomeEstoque);
        btnHomeEstoque.setBounds(1240, 30, 40, 40);

        lblLogo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N
        tabEstoqueProduto.add(lblLogo1);
        lblLogo1.setBounds(10, 10, 200, 94);

        tabProduto.addTab("Estoque de Produto", tabEstoqueProduto);

        lblFoto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnProcurar.setBackground(new java.awt.Color(0, 0, 204));
        btnProcurar.setFont(new java.awt.Font("Arial Unicode MS", 1, 14)); // NOI18N
        btnProcurar.setForeground(new java.awt.Color(255, 255, 255));
        btnProcurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Iconedocuments.png"))); // NOI18N
        btnProcurar.setToolTipText("Procurar");
        btnProcurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcurarActionPerformed(evt);
            }
        });

        lblProduto.setText("Nome da foto:");
        lblProduto.setName("lblModelo"); // NOI18N

        lblTituloEstoque1.setFont(new java.awt.Font("Arial Unicode MS", 1, 24)); // NOI18N
        lblTituloEstoque1.setForeground(new java.awt.Color(102, 102, 102));
        lblTituloEstoque1.setText("Cadastro de Foto");

        lblProduto1.setText("Selecione o produto:");
        lblProduto1.setName("lblModelo"); // NOI18N

        btnSalvarFotos.setBackground(new java.awt.Color(0, 0, 204));
        btnSalvarFotos.setFont(new java.awt.Font("Arial Unicode MS", 1, 14)); // NOI18N
        btnSalvarFotos.setForeground(new java.awt.Color(255, 255, 255));
        btnSalvarFotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/IconeSalvar.png"))); // NOI18N
        btnSalvarFotos.setToolTipText("Salvar");
        btnSalvarFotos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarFotosActionPerformed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(0, 0, 204));
        btnCancelar.setFont(new java.awt.Font("Arial Unicode MS", 1, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/IconeCancelar.png"))); // NOI18N
        btnCancelar.setToolTipText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnHomeFoto.setBackground(new java.awt.Color(204, 0, 0));
        btnHomeFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Iconehome.png"))); // NOI18N
        btnHomeFoto.setToolTipText("Home");

        btnVerFotosCadastradas.setBackground(new java.awt.Color(0, 0, 204));
        btnVerFotosCadastradas.setFont(new java.awt.Font("Arial Unicode MS", 1, 14)); // NOI18N
        btnVerFotosCadastradas.setForeground(new java.awt.Color(255, 255, 255));
        btnVerFotosCadastradas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/IconeEye.png"))); // NOI18N
        btnVerFotosCadastradas.setToolTipText("Ver Fotos Cadastradas");
        btnVerFotosCadastradas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerFotosCadastradasActionPerformed(evt);
            }
        });

        lblLogo2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 0, 51));
        jLabel18.setText("* Campos Obrigatórios");

        jLabel14.setForeground(new java.awt.Color(255, 0, 0));
        jLabel14.setText("*");

        jLabel15.setForeground(new java.awt.Color(255, 0, 0));
        jLabel15.setText("*");

        javax.swing.GroupLayout tabCadastroFotoProdutoLayout = new javax.swing.GroupLayout(tabCadastroFotoProduto);
        tabCadastroFotoProduto.setLayout(tabCadastroFotoProdutoLayout);
        tabCadastroFotoProdutoLayout.setHorizontalGroup(
            tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblLogo2)
                        .addGap(20, 20, 20)
                        .addComponent(lblTituloEstoque1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(750, 750, 750)
                        .addComponent(btnHomeFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                        .addGap(480, 480, 480)
                        .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(lblIconFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                        .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                                .addGap(148, 148, 148)
                                .addComponent(btnProcurar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                                .addGap(83, 83, 83)
                                .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel18)
                                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                                        .addComponent(lblProduto1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(txtCaminho, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(btnSalvarFotos, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnVerFotosCadastradas, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabCadastroFotoProdutoLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(lblProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNomeFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 432, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(75, 75, 75))
        );
        tabCadastroFotoProdutoLayout.setVerticalGroup(
            tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLogo2)
                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblTituloEstoque1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(btnHomeFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblProduto1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(lblProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNomeFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProcurar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(txtCaminho, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSalvarFotos, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVerFotosCadastradas, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(tabCadastroFotoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabCadastroFotoProdutoLayout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(lblIconFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jLabel18))
        );

        tabProduto.addTab("Cadastro de Foto", tabCadastroFotoProduto);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(tabProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 1360, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(tabProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 650, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        setSize(new java.awt.Dimension(1322, 671));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if(validar())
        {
            obterProduto();
            String erro = produtoData.saveProduto(produto);
            if(erro==null)
            {
                atualizarTabela(produtoData.selectProduto());
                JOptionPane.showMessageDialog(null, "Produto cadastrado com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
                if(saveProduto==true)
                {
                      JOptionPane.showMessageDialog(null, "Produto cadastrado com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);

                      int opcaoEscolhida = JOptionPane.showConfirmDialog(null, "Deseja cadastrar a foto deste produto agora?","Confirmação",JOptionPane.YES_OPTION);  
                      if(opcaoEscolhida == JOptionPane.YES_OPTION)
                      {
                          tabProduto.setSelectedIndex(2);
                         for (int i = 0; i < cbxProduto.getItemCount(); i++) 
                         {
                              Produto p2 = (Produto)(cbxProduto.getItemAt(i));
                              if (produto.getIdProduto() == p2.getIdProduto()) 
                              {
                                  cbxProduto.setSelectedIndex(i);

                              }            
                         }
                      }
                }
                
                else
                {
                    JOptionPane.showMessageDialog(null, "Produto alterado com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
                    saveProduto = true;
                } 
            }
            else
            {
                JOptionPane.showMessageDialog(null, erro);
            }
            atualizarTabela(produtoData.selectProduto());
            limparFormulario();
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        if (tblProdutos.getSelectedRow() == -1)
        {
            JOptionPane.showMessageDialog(null, "Selecione um produto para excluir!", "Alerta", 2);
        }
        else
        {
            int opcaoEscolhida = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir esse produto?","Confirmação",JOptionPane.YES_OPTION);
            if(opcaoEscolhida == JOptionPane.YES_OPTION)
            {
                Object selectedObject = (Object) tblProdutos.getModel().getValueAt(tblProdutos.getSelectedRow(), 0);
                Produto p = Produto.class.cast(selectedObject);

                produtoData.deleteProduto(p);
                atualizarTabela(produtoData.selectProduto());
                limparFormulario();
                JOptionPane.showMessageDialog(null, "Produto excluído com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if(tblProdutos.getSelectedRow()==-1)
        {
            JOptionPane.showMessageDialog(null, "Selecione um produto para editar!", "Alerta", 2);
            return;
        }
        else
        {
            limparFormulario();
            Produto p = (Produto) tblProdutos.getValueAt(tblProdutos.getSelectedRow(), 0);
            atribuirProduto(p);
            atualizarTabela(produtoData.selectProduto());
            saveProduto =false;
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limparFormulario(); 
        saveProduto = true;
    }//GEN-LAST:event_btnNovoActionPerformed

    private void txtPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyReleased
        
        atualizarTabela(produtoData.getProduto(txtPesquisa.getText()));
    }//GEN-LAST:event_txtPesquisaKeyReleased
//-------------------------------------------------------------------------------------------------------------
    //IMAGEM DO PRODUTO:
//-------------------------------------------------------------------------------------------------------------

    public boolean getSaveFoto() {
        return saveFoto;
    }

    public void setSaveFoto(boolean saveFoto) {
        this.saveFoto = saveFoto;
    }
    
    private void inserirProdutoCbx()
    {       
        cbxProduto.removeAllItems();
        for(Object p : produtoData.selectProduto())
        {
      
            cbxProduto.addItem(p);
        }
        Foto f = new Foto();
        f.setProduto((Produto) cbxProduto.getSelectedItem());
        
    }
    private void limparFormularioFoto()
    {
        idFoto=0;
        txtCaminho.setText("");
        txtNomeFoto.setText("");
        cbxProduto.setSelectedIndex(-1);
        lblIconFoto.setVisible(true);
        lblFoto.setIcon(new javax.swing.ImageIcon(""));
        foto = new Foto();
    }
    private boolean validarFoto()
    {
        if(cbxProduto.getSelectedItem()==null)
        {
            JOptionPane.showMessageDialog(cbxProduto, "Selecione um produto para adicionar foto!","Alerta",2);
            return false;
        }
        if(txtNomeFoto.getText().isEmpty() || txtNomeFoto.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtCodigoBarras, "Insira o nome da foto do produto!","Alerta",2);
            return false;
        }
        if(txtCaminho.getText().isEmpty() || txtCaminho.getText().equals(null))
        {
            JOptionPane.showMessageDialog(txtCodigoBarras, "Insira o caminho para a foto do produto!","Alerta",2);
            return false;
        }
        return true;
    }
    private void setIconeQuadroDeFoto()
    {
        lblIconFoto.setIcon(new javax.swing.ImageIcon("C:\\Users\\Leandro\\Documents\\trab_diurno_sistemas_2015\\"
         + "LojaDiurno\\src\\solutionItems\\IconeFoto.png"));
    }
    public void atribuirFoto(Foto f)
    {
        limparFormularioFoto();
        lblIconFoto.setVisible(false);
        idFoto =  f.getIdFoto();
        txtNomeFoto.setText(""+f.getNomeFisico());
        txtCaminho.setText(""+f.getCaminho());
        lblFoto.setIcon(new javax.swing.ImageIcon(""+txtCaminho.getText()));
        for (int i = 0; i < cbxProduto.getItemCount(); i++) 
        {
            Produto p = (Produto)(cbxProduto.getItemAt(i));
            if (p.getIdProduto() == f.getProduto().getIdProduto()) 
            {
                cbxProduto.setSelectedIndex(i);
            }            
        }
    }
    private void obterFoto()
    {
       foto = new Foto();
       foto.setIdFoto(idFoto);
       foto.setNomeFisico(txtNomeFoto.getText());
       foto.setCaminho(txtCaminho.getText());
       foto.setProduto((Produto)cbxProduto.getSelectedItem());
    }
    private void setImagemProdutoQuadroDeFoto()
    {
            JFileChooser jf = new JFileChooser();
            jf.showOpenDialog(null);
            File f = jf.getSelectedFile();
            String fileName = f.getAbsolutePath();
            txtCaminho.setText(fileName);
            lblFoto.setIcon(new javax.swing.ImageIcon(""+txtCaminho.getText())); 
    }
    private void btnProcurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcurarActionPerformed
           setImagemProdutoQuadroDeFoto();
            lblIconFoto.setVisible(false);
    }//GEN-LAST:event_btnProcurarActionPerformed

    private void btnSalvarFotosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarFotosActionPerformed
        if(validarFoto())
        {
            obterFoto();
            String erro = fotoData.saveFoto(foto);
            if(erro==null)
            {
                if(saveFoto==true)
                {
                    JOptionPane.showMessageDialog(null, "A foto do produto foi cadastrada com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
                    lblIconFoto.setVisible(true);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "A foto do produto foi alterada com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
                    lblIconFoto.setVisible(true);
                    saveFoto=true;
                }
                
            }
            else
            {
                JOptionPane.showMessageDialog(null, erro);
                saveFoto=true;
            }
            limparFormularioFoto();
        }
    }//GEN-LAST:event_btnSalvarFotosActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        limparFormularioFoto();
        lblIconFoto.setVisible(true);
        saveFoto=true;
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnVerFotosCadastradasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerFotosCadastradasActionPerformed
           FormProdutoFotos fpf = new FormProdutoFotos(this);
           fpf.setVisible(true);
    }//GEN-LAST:event_btnVerFotosCadastradasActionPerformed
//------------------------------------------------------------------------------------------------------------
    // CADASTRO DE PRODUTO:
    private void btnAlterarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarFotoActionPerformed
       if(tblProdutos.getSelectedRow()==-1)
        {
            JOptionPane.showMessageDialog(null, "Selecione um produto para editar a foto!", "Alerta", 2);
            return;
        }
        else
        {
            Produto p = (Produto) tblProdutos.getValueAt(tblProdutos.getSelectedRow(), 0);
            tabProduto.setSelectedIndex(2);
             for (int i = 0; i < cbxProduto.getItemCount(); i++) 
             {
                Produto p2 = (Produto)(cbxProduto.getItemAt(i));
                if (p.getIdProduto() == p2.getIdProduto()) 
                {
                    cbxProduto.setSelectedIndex(i);
                    
                }            
             }
            
        }
    }//GEN-LAST:event_btnAlterarFotoActionPerformed
  //---------------------------------------------------------------------------------------------------------------
  //ESTOQUE DE PRODUTO:
    
    
    //-------------------------------------------------------------------------------------------------------------
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormProduto().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterarFoto;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnHomeCadastro;
    private javax.swing.JButton btnHomeEstoque;
    private javax.swing.JButton btnHomeFoto;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnProcurar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnSalvarFotos;
    private javax.swing.JButton btnVerFotosCadastradas;
    private javax.swing.JComboBox cbxMarca;
    private javax.swing.JComboBox cbxProduto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblAltura;
    private javax.swing.JLabel lblDescrição1;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JLabel lblIconFoto;
    private javax.swing.JLabel lblIconePesquisa;
    private javax.swing.JLabel lblLargura;
    private javax.swing.JLabel lblLargura1;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblLogo1;
    private javax.swing.JLabel lblLogo2;
    private javax.swing.JLabel lblLucro;
    private javax.swing.JLabel lblMarca;
    private javax.swing.JLabel lblModelo1;
    private javax.swing.JLabel lblModelo3;
    private javax.swing.JLabel lblPeso1;
    private javax.swing.JLabel lblPesquisa;
    private javax.swing.JLabel lblProduto;
    private javax.swing.JLabel lblProduto1;
    private javax.swing.JLabel lblQtde3;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblTituloEstoque;
    private javax.swing.JLabel lblTituloEstoque1;
    private javax.swing.JLabel lblValorCompra;
    private javax.swing.JLabel lblValorVenda2;
    private javax.swing.JPanel tabCadastroFotoProduto;
    private javax.swing.JPanel tabCadastroProduto;
    private javax.swing.JPanel tabEstoqueProduto;
    private javax.swing.JTabbedPane tabProduto;
    private javax.swing.JTable tblProdutos;
    private javax.swing.JTable tblProdutosEstoque;
    private javax.swing.JTextField txtAltura;
    private javax.swing.JTextField txtCaminho;
    private javax.swing.JTextField txtCodigoBarras;
    private javax.swing.JTextField txtComprimento;
    private javax.swing.JTextField txtDescricao;
    private javax.swing.JTextField txtLargura;
    private javax.swing.JTextField txtLucro;
    private javax.swing.JTextField txtModelo;
    private javax.swing.JTextField txtNomeFoto;
    private javax.swing.JTextField txtPeso;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JTextField txtQtde;
    private javax.swing.JTextField txtValorCompra;
    private javax.swing.JTextField txtValorVenda;
    // End of variables declaration//GEN-END:variables
}
