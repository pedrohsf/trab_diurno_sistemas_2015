
package UI;

import Data.FotoData;
import business.Foto;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Leandro
 */
public class FormProdutoFotos extends javax.swing.JFrame 
{
    private Foto foto;
    private FotoData fotoData;
    private Short idFoto;
    private FormProduto fp;

    public FormProdutoFotos(FormProduto fp) 
    {
        initComponents();
        fotoData = new FotoData();
        foto = new Foto();
        atualizarTabela(fotoData.selectFoto());
        this.fp = fp;
    }
    
    private void atribuirFoto(Foto f)
    {
        idFoto =  f.getIdFoto();
        
       
    }
    private void atualizarTabela(List<Foto> fotos)
    {
        tblFotos = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Produto","Foto", "Caminho"}, 0)
        {
            private static final long serialVersionUID = 1L;
            boolean[] canEdit = new boolean[]
            {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) 
            {
                return canEdit[columnIndex];
            }
        };
 
        model.setRowCount(0);
       
        for (Foto f : fotos) 
        {
            
            model.addRow(new Object[]
            { 
                f,
                f.getProduto().getDescricao(),
                f.getNomeFisico(),
                f.getCaminho(),
            });
        }
        tblFotos.setModel(model);
       
        tblFotos.addMouseListener(new MouseAdapter() 
        {
            public void mouseClicked(MouseEvent e) 
            {
                if (e.getClickCount() == 2) 
                {
                    btnEditarActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        tblFotos.setAutoCreateRowSorter(true);
        tblFotos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblFotos.getColumnModel().getColumn(0).setMinWidth(0);
        tblFotos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblFotos.getColumnModel().getColumn(1).setPreferredWidth(210);
        tblFotos.getColumnModel().getColumn(2).setPreferredWidth(210);
        tblFotos.getColumnModel().getColumn(3).setPreferredWidth(384);     
        tblFotos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
       jScrollPane1.setViewportView(tblFotos);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabCadastroFotos = new javax.swing.JTabbedPane();
        pcgCadastro = new javax.swing.JPanel();
        lblTituloEstoque1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblFotos = new javax.swing.JTable();
        lblPesquisa = new javax.swing.JLabel();
        lblIconePesquisa = new javax.swing.JLabel();
        txtPesquisa = new javax.swing.JTextField();
        btnEditar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        tabCadastroFotos.setBackground(new java.awt.Color(0, 0, 204));

        pcgCadastro.setLayout(null);

        lblTituloEstoque1.setFont(new java.awt.Font("Arial Unicode MS", 1, 24)); // NOI18N
        lblTituloEstoque1.setForeground(new java.awt.Color(102, 102, 102));
        lblTituloEstoque1.setText("Fotos Cadastradas");
        pcgCadastro.add(lblTituloEstoque1);
        lblTituloEstoque1.setBounds(240, 20, 220, 62);

        tblFotos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblFotos);

        pcgCadastro.add(jScrollPane1);
        jScrollPane1.setBounds(30, 130, 810, 230);

        lblPesquisa.setText("Digite o nome da foto para pesquisar:");
        lblPesquisa.setName("lblModelo"); // NOI18N
        pcgCadastro.add(lblPesquisa);
        lblPesquisa.setBounds(30, 100, 220, 20);

        lblIconePesquisa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/IconePesquisar.png"))); // NOI18N
        pcgCadastro.add(lblIconePesquisa);
        lblIconePesquisa.setBounds(240, 90, 34, 30);

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyReleased(evt);
            }
        });
        pcgCadastro.add(txtPesquisa);
        txtPesquisa.setBounds(280, 100, 560, 25);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setToolTipText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        pcgCadastro.add(btnEditar);
        btnEditar.setBounds(30, 370, 120, 30);

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnExcluir.setText("Excluir");
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        pcgCadastro.add(btnExcluir);
        btnExcluir.setBounds(720, 370, 120, 30);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N
        pcgCadastro.add(jLabel5);
        jLabel5.setBounds(10, 0, 200, 94);

        tabCadastroFotos.addTab("Fotos Cadastradas", pcgCadastro);

        getContentPane().add(tabCadastroFotos);
        tabCadastroFotos.setBounds(0, 0, 880, 490);

        setSize(new java.awt.Dimension(882, 501));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if(tblFotos.getSelectedRow()==-1)
        {
            JOptionPane.showMessageDialog(null, "Selecione uma foto para editar!", "Alerta", 2);
            return;
        }
        else
        {
            Foto f = (Foto) tblFotos.getValueAt(tblFotos.getSelectedRow(), 0);
            fp.atribuirFoto(f);
            fp.setSaveFoto(false);
            this.dispose();
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
       if (tblFotos.getSelectedRow() == -1)
        {
            JOptionPane.showMessageDialog(null, "Selecione uma foto para excluir!", "Alerta", 2);
        }
        else
        {
            int opcaoEscolhida = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir essa foto?","Confirmação",JOptionPane.YES_OPTION);
            if(opcaoEscolhida == JOptionPane.YES_OPTION)
            {
                Object selectedObject = (Object) tblFotos.getModel().getValueAt(tblFotos.getSelectedRow(), 0);
                Foto f = Foto.class.cast(selectedObject);

                fotoData.deleteFoto(f);
                atualizarTabela(fotoData.selectFoto());
                JOptionPane.showMessageDialog(null, "Foto excluída com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void txtPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyReleased

        atualizarTabela(fotoData.getFoto(txtPesquisa.getText()));
    }//GEN-LAST:event_txtPesquisaKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormProdutoFotos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormProdutoFotos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormProdutoFotos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormProdutoFotos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormProdutoFotos(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIconePesquisa;
    private javax.swing.JLabel lblPesquisa;
    private javax.swing.JLabel lblTituloEstoque1;
    private javax.swing.JPanel pcgCadastro;
    private javax.swing.JTabbedPane tabCadastroFotos;
    private javax.swing.JTable tblFotos;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables
}
