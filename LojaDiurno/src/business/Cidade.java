package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cidade", catalog = "lojadiurno"
)
public class Cidade implements java.io.Serializable {

    private Integer idCidade;
    private Estado estado;
    private String cidade;
    private String cep;
    private Set<Bairro> bairros = new HashSet<Bairro>(0);

    public Cidade() {
    }

    public Cidade(Estado estado, String cidade, String cep) {
        this.estado = estado;
        this.cidade = cidade;
        this.cep = cep;
    }

    public Cidade(Estado estado, String cidade, String cep, Set<Bairro> bairros) {
        this.estado = estado;
        this.cidade = cidade;
        this.cep = cep;
        this.bairros = bairros;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdCidade", unique = true, nullable = false)
    public Integer getIdCidade() {
        return this.idCidade;
    }

    public void setIdCidade(Integer idCidade) {
        this.idCidade = idCidade;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UF", nullable = false)
    public Estado getEstado() {
        return this.estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Column(name = "Cidade", nullable = false, length = 200)
    public String getCidade() {
        return this.cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @Column(name = "CEP", nullable = false, length = 20)
    public String getCep() {
        return this.cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cidade")
    public Set<Bairro> getBairros() {
        return this.bairros;
    }

    public void setBairros(Set<Bairro> bairros) {
        this.bairros = bairros;
    }

}
