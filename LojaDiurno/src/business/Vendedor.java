package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "vendedor", catalog = "lojadiurno"
)
public class Vendedor implements java.io.Serializable {

    private Integer idVendedor;
    private Pessoa pessoa;
    private Set<Venda> vendas = new HashSet<Venda>(0);
    private Set<Ordemservico> ordemservicos = new HashSet<Ordemservico>(0);

    public Vendedor() {
    }

    public Vendedor(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Vendedor(Pessoa pessoa, Set<Venda> vendas, Set<Ordemservico> ordemservicos) {
        this.pessoa = pessoa;
        this.vendas = vendas;
        this.ordemservicos = ordemservicos;
    }

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "pessoa"))
    @Id
    @GeneratedValue(generator = "generator")

    @Column(name = "IdVendedor", unique = true, nullable = false)
    public Integer getIdVendedor() {
        return this.idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Pessoa getPessoa() {
        return this.pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "vendedor")
    public Set<Venda> getVendas() {
        return this.vendas;
    }

    public void setVendas(Set<Venda> vendas) {
        this.vendas = vendas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "vendedor")
    public Set<Ordemservico> getOrdemservicos() {
        return this.ordemservicos;
    }

    public void setOrdemservicos(Set<Ordemservico> ordemservicos) {
        this.ordemservicos = ordemservicos;
    }

}
