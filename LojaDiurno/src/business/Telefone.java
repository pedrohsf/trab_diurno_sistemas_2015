package business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "telefone", catalog = "lojadiurno"
)
public class Telefone implements java.io.Serializable {

    private Integer idTelefone;
    private Pessoa pessoa;
    private Tipotelefone tipotelefone;
    private String numeroTelefone;
    private String operadora;

    public Telefone() {
    }

    public Telefone(Pessoa pessoa, Tipotelefone tipotelefone, String numeroTelefone, String operadora) {
        this.pessoa = pessoa;
        this.tipotelefone = tipotelefone;
        this.numeroTelefone = numeroTelefone;
        this.operadora = operadora;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdTelefone", unique = true, nullable = false)
    public Integer getIdTelefone() {
        return this.idTelefone;
    }

    public void setIdTelefone(Integer idTelefone) {
        this.idTelefone = idTelefone;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdPessoa", nullable = false)
    public Pessoa getPessoa() {
        return this.pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdTipoTelefone", nullable = false)
    public Tipotelefone getTipotelefone() {
        return this.tipotelefone;
    }

    public void setTipotelefone(Tipotelefone tipotelefone) {
        this.tipotelefone = tipotelefone;
    }

    @Column(name = "NumeroTelefone", nullable = false, length = 14)
    public String getNumeroTelefone() {
        return this.numeroTelefone;
    }

    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }

    @Column(name = "Operadora", nullable = false)
    public String getOperadora() {
        return this.operadora;
    }

    public void setOperadora(String operadora) {
        this.operadora = operadora;
    }

}
