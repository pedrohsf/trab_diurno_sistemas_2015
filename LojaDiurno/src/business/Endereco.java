package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "endereco", catalog = "lojadiurno"
)
public class Endereco implements java.io.Serializable {

    private Integer idEndereco;
    private Bairro bairro;
    private String nomeEndereco;
    private String enderecoCompleto;
    private String cep;
    private String logradouro;
    private Set<Enderecopessoa> enderecopessoas = new HashSet<Enderecopessoa>(0);

    public Endereco() {
    }

    public Endereco(Bairro bairro, String nomeEndereco, String enderecoCompleto, String cep, String logradouro) {
        this.bairro = bairro;
        this.nomeEndereco = nomeEndereco;
        this.enderecoCompleto = enderecoCompleto;
        this.cep = cep;
        this.logradouro = logradouro;
    }

    public Endereco(Bairro bairro, String nomeEndereco, String enderecoCompleto, String cep, String logradouro, Set<Enderecopessoa> enderecopessoas) {
        this.bairro = bairro;
        this.nomeEndereco = nomeEndereco;
        this.enderecoCompleto = enderecoCompleto;
        this.cep = cep;
        this.logradouro = logradouro;
        this.enderecopessoas = enderecopessoas;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdEndereco", unique = true, nullable = false)
    public Integer getIdEndereco() {
        return this.idEndereco;
    }

    public void setIdEndereco(Integer idEndereco) {
        this.idEndereco = idEndereco;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdBairro", nullable = false)
    public Bairro getBairro() {
        return this.bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    @Column(name = "NomeEndereco", nullable = false, length = 200)
    public String getNomeEndereco() {
        return this.nomeEndereco;
    }

    public void setNomeEndereco(String nomeEndereco) {
        this.nomeEndereco = nomeEndereco;
    }

    @Column(name = "EnderecoCompleto", nullable = false, length = 200)
    public String getEnderecoCompleto() {
        return this.enderecoCompleto;
    }

    public void setEnderecoCompleto(String enderecoCompleto) {
        this.enderecoCompleto = enderecoCompleto;
    }

    @Column(name = "CEP", nullable = false, length = 20)
    public String getCep() {
        return this.cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Column(name = "Logradouro", nullable = false, length = 50)
    public String getLogradouro() {
        return this.logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "endereco")
    public Set<Enderecopessoa> getEnderecopessoas() {
        return this.enderecopessoas;
    }

    public void setEnderecopessoas(Set<Enderecopessoa> enderecopessoas) {
        this.enderecopessoas = enderecopessoas;
    }

}
