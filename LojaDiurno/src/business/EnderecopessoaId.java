package business;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EnderecopessoaId implements java.io.Serializable {

    private int idEndereco;
    private int idPessoa;

    public EnderecopessoaId() {
    }

    public EnderecopessoaId(int idEndereco, int idPessoa) {
        this.idEndereco = idEndereco;
        this.idPessoa = idPessoa;
    }

    @Column(name = "IdEndereco", nullable = false)
    public int getIdEndereco() {
        return this.idEndereco;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }

    @Column(name = "IdPessoa", nullable = false)
    public int getIdPessoa() {
        return this.idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof EnderecopessoaId)) {
            return false;
        }
        EnderecopessoaId castOther = (EnderecopessoaId) other;

        return (this.getIdEndereco() == castOther.getIdEndereco())
                && (this.getIdPessoa() == castOther.getIdPessoa());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getIdEndereco();
        result = 37 * result + this.getIdPessoa();
        return result;
    }

}
