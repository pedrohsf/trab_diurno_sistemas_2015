package business;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ItemcompraId implements java.io.Serializable {

    private int idCompra;
    private int idProduto;

    public ItemcompraId() {
    }

    public ItemcompraId(int idCompra, int idProduto) {
        this.idCompra = idCompra;
        this.idProduto = idProduto;
    }

    @Column(name = "IdCompra", nullable = false)
    public int getIdCompra() {
        return this.idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    @Column(name = "IdProduto", nullable = false)
    public int getIdProduto() {
        return this.idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof ItemcompraId)) {
            return false;
        }
        ItemcompraId castOther = (ItemcompraId) other;

        return (this.getIdCompra() == castOther.getIdCompra())
                && (this.getIdProduto() == castOther.getIdProduto());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getIdCompra();
        result = 37 * result + this.getIdProduto();
        return result;
    }

}
