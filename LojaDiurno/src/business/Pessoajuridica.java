package business;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "pessoajuridica", catalog = "lojadiurno", uniqueConstraints = @UniqueConstraint(columnNames = "CNPJ")
)
public class Pessoajuridica implements java.io.Serializable {

    private Integer idPessoaJuridica;
    private Pessoa pessoa;
    private String razaoSocial;
    private String cnpj;
    private String incricaoEstadual;
    private Fornecedor fornecedor;

    public Pessoajuridica() {
    }

    public Pessoajuridica(Pessoa pessoa, String razaoSocial, String cnpj) {
        this.pessoa = pessoa;
        this.razaoSocial = razaoSocial;
        this.cnpj = cnpj;
    }

    public Pessoajuridica(Pessoa pessoa, String razaoSocial, String cnpj, String incricaoEstadual, Fornecedor fornecedor) {
        this.pessoa = pessoa;
        this.razaoSocial = razaoSocial;
        this.cnpj = cnpj;
        this.incricaoEstadual = incricaoEstadual;
        this.fornecedor = fornecedor;
    }

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "pessoa"))
    @Id
    @GeneratedValue(generator = "generator")

    @Column(name = "IdPessoaJuridica", unique = true, nullable = false)
    public Integer getIdPessoaJuridica() {
        return this.idPessoaJuridica;
    }

    public void setIdPessoaJuridica(Integer idPessoaJuridica) {
        this.idPessoaJuridica = idPessoaJuridica;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    public Pessoa getPessoa() {
        return this.pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Column(name = "RazaoSocial", nullable = false, length = 150)
    public String getRazaoSocial() {
        return this.razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    @Column(name = "CNPJ", unique = true, nullable = false, length = 18)
    public String getCnpj() {
        return this.cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    @Column(name = "IncricaoEstadual", length = 12)
    public String getIncricaoEstadual() {
        return this.incricaoEstadual;
    }

    public void setIncricaoEstadual(String incricaoEstadual) {
        this.incricaoEstadual = incricaoEstadual;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "pessoajuridica", cascade = CascadeType.ALL)
    public Fornecedor getFornecedor() {
        return this.fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

}
