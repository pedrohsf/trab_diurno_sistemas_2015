package business;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "produto", catalog = "lojadiurno"
)
public class Produto implements java.io.Serializable {

    private Integer idProduto;
    private Marca marca;
    private String descricao;
    private String modelo;
    private String codigoBarras;
    private BigDecimal peso;
    private BigDecimal valorVenda;
    private BigDecimal percentualLucro;
    private BigDecimal valorCompra;
    private BigDecimal quantidadeInicial;
    private BigDecimal altura;
    private BigDecimal largura;
    private BigDecimal comprimento;
    private Set<Vigenciapreco> vigenciaprecos = new HashSet(0);
    private Set<Itemvenda> itemvendas = new HashSet(0);
    private Set<Movimentacao> movimentacaos = new HashSet<Movimentacao>(0);
    private Set<Itemordemservico> itemordemservicos = new HashSet<Itemordemservico>(0);
    private Set<Itemcompra> itemcompras = new HashSet<Itemcompra>(0);
    private Set<Itemproduto> itemprodutos = new HashSet<Itemproduto>(0);
    private Set<Foto> fotos = new HashSet<Foto>(0);

    public Produto() {
    }

    public Produto(Marca marca, String descricao, String modelo, String codigoBarras, BigDecimal peso, BigDecimal valorVenda, BigDecimal percentualLucro, BigDecimal valorCompra, BigDecimal quantidadeInicial, BigDecimal altura, BigDecimal largura, BigDecimal comprimento) {
        this.marca = marca;
        this.descricao = descricao;
        this.modelo = modelo;
        this.codigoBarras = codigoBarras;
        this.peso = peso;
        this.valorVenda = valorVenda;
        this.percentualLucro = percentualLucro;
        this.valorCompra = valorCompra;
        this.quantidadeInicial = quantidadeInicial;
        this.altura = altura;
        this.largura = largura;
        this.comprimento = comprimento;
    }

    public Produto(Marca marca, String descricao, String modelo, String codigoBarras, BigDecimal peso, BigDecimal valorVenda, BigDecimal percentualLucro, BigDecimal valorCompra, BigDecimal quantidadeInicial, BigDecimal altura, BigDecimal largura, BigDecimal comprimento, Set<Vigenciapreco> vigenciaprecos, Set<Itemvenda> itemvendas, Set<Movimentacao> movimentacaos, Set<Itemordemservico> itemordemservicos, Set<Itemcompra> itemcompras, Set<Itemproduto> itemprodutos, Set<Foto> fotos) {
        this.marca = marca;
        this.descricao = descricao;
        this.modelo = modelo;
        this.codigoBarras = codigoBarras;
        this.peso = peso;
        this.valorVenda = valorVenda;
        this.percentualLucro = percentualLucro;
        this.valorCompra = valorCompra;
        this.quantidadeInicial = quantidadeInicial;
        this.altura = altura;
        this.largura = largura;
        this.comprimento = comprimento;
        this.vigenciaprecos = vigenciaprecos;
        this.itemvendas = itemvendas;
        this.movimentacaos = movimentacaos;
        this.itemordemservicos = itemordemservicos;
        this.itemcompras = itemcompras;
        this.itemprodutos = itemprodutos;
        this.fotos = fotos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdProduto", unique = true, nullable = false)
    public Integer getIdProduto() {
        return this.idProduto;
    }

    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdMarca", nullable = false)
    public Marca getMarca() {
        return this.marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    @Column(name = "Descricao", nullable = false, length = 65535)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name = "Modelo", nullable = false, length = 50)
    public String getModelo() {
        return this.modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Column(name = "CodigoBarras", nullable = false, length = 100)
    public String getCodigoBarras() {
        return this.codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    @Column(name = "Peso", nullable = false, precision = 10, scale = 3)
    public BigDecimal getPeso() {
        return this.peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso = peso;
    }

    @Column(name = "ValorVenda", nullable = false, precision = 10)
    public BigDecimal getValorVenda() {
        return this.valorVenda;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }

    @Column(name = "PercentualLucro", nullable = false, precision = 10)
    public BigDecimal getPercentualLucro() {
        return this.percentualLucro;
    }

    public void setPercentualLucro(BigDecimal percentualLucro) {
        this.percentualLucro = percentualLucro;
    }

    @Column(name = "ValorCompra", nullable = false, precision = 10)
    public BigDecimal getValorCompra() {
        return this.valorCompra;
    }

    public void setValorCompra(BigDecimal valorCompra) {
        this.valorCompra = valorCompra;
    }

    @Column(name = "QuantidadeInicial", nullable = false, precision = 10)
    public BigDecimal getQuantidadeInicial() {
        return this.quantidadeInicial;
    }

    public void setQuantidadeInicial(BigDecimal quantidadeInicial) {
        this.quantidadeInicial = quantidadeInicial;
    }

    @Column(name = "Altura", nullable = false, precision = 10)
    public BigDecimal getAltura() {
        return this.altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    @Column(name = "Largura", nullable = false, precision = 10)
    public BigDecimal getLargura() {
        return this.largura;
    }

    public void setLargura(BigDecimal largura) {
        this.largura = largura;
    }

    @Column(name = "Comprimento", nullable = false, precision = 10)
    public BigDecimal getComprimento() {
        return this.comprimento;
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = comprimento;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "produto")
    public Set<Vigenciapreco> getVigenciaprecos() {
        return this.vigenciaprecos;
    }

    public void setVigenciaprecos(Set<Vigenciapreco> vigenciaprecos) {
        this.vigenciaprecos = vigenciaprecos;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "produto")
    public Set<Itemvenda> getItemvendas() {
        return this.itemvendas;
    }

    public void setItemvendas(Set<Itemvenda> itemvendas) {
        this.itemvendas = itemvendas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "produto")
    public Set<Movimentacao> getMovimentacaos() {
        return this.movimentacaos;
    }

    public void setMovimentacaos(Set<Movimentacao> movimentacaos) {
        this.movimentacaos = movimentacaos;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "produto")
    public Set<Itemordemservico> getItemordemservicos() {
        return this.itemordemservicos;
    }

    public void setItemordemservicos(Set<Itemordemservico> itemordemservicos) {
        this.itemordemservicos = itemordemservicos;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "produto")
    public Set<Itemcompra> getItemcompras() {
        return this.itemcompras;
    }

    public void setItemcompras(Set<Itemcompra> itemcompras) {
        this.itemcompras = itemcompras;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "produto")
    public Set<Itemproduto> getItemprodutos() {
        return this.itemprodutos;
    }

    public void setItemprodutos(Set<Itemproduto> itemprodutos) {
        this.itemprodutos = itemprodutos;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "produto")
    public Set<Foto> getFotos() {
        return this.fotos;
    }

    public void setFotos(Set<Foto> fotos) {
        this.fotos = fotos;
    }
    @Override
    public String toString()
    {
        return this.descricao;
    }
}
