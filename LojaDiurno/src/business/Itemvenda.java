package business;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "itemvenda", catalog = "lojadiurno"
)
public class Itemvenda implements java.io.Serializable {

    private ItemvendaId id;
    private Produto produto;
    private Venda venda;
    private BigDecimal quantidade;
    private BigDecimal valorItem;

    public Itemvenda() {
    }

    public Itemvenda(ItemvendaId id, Produto produto, Venda venda, BigDecimal quantidade, BigDecimal valorItem) {
        this.id = id;
        this.produto = produto;
        this.venda = venda;
        this.quantidade = quantidade;
        this.valorItem = valorItem;
    }

    @EmbeddedId

    @AttributeOverrides({
        @AttributeOverride(name = "idVenda", column = @Column(name = "IdVenda", nullable = false)),
        @AttributeOverride(name = "idProduto", column = @Column(name = "IdProduto", nullable = false))})
    public ItemvendaId getId() {
        return this.id;
    }

    public void setId(ItemvendaId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdProduto", nullable = false, insertable = false, updatable = false)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdVenda", nullable = false, insertable = false, updatable = false)
    public Venda getVenda() {
        return this.venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @Column(name = "Quantidade", nullable = false, precision = 10)
    public BigDecimal getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    @Column(name = "ValorItem", nullable = false, precision = 10)
    public BigDecimal getValorItem() {
        return this.valorItem;
    }

    public void setValorItem(BigDecimal valorItem) {
        this.valorItem = valorItem;
    }

}
