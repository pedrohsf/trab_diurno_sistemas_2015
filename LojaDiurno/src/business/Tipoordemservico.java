package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tipoordemservico", catalog = "lojadiurno"
)
public class Tipoordemservico implements java.io.Serializable {

    private Integer idTipoOrdemServico;
    private String descricao;
    private Set<Ordemservico> ordemservicos = new HashSet<Ordemservico>(0);

    public Tipoordemservico() {
    }

    public Tipoordemservico(String descricao) {
        this.descricao = descricao;
    }

    public Tipoordemservico(String descricao, Set<Ordemservico> ordemservicos) {
        this.descricao = descricao;
        this.ordemservicos = ordemservicos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdTipoOrdemServico", unique = true, nullable = false)
    public Integer getIdTipoOrdemServico() {
        return this.idTipoOrdemServico;
    }

    public void setIdTipoOrdemServico(Integer idTipoOrdemServico) {
        this.idTipoOrdemServico = idTipoOrdemServico;
    }

    @Column(name = "Descricao", nullable = false, length = 80)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tipoordemservico")
    public Set<Ordemservico> getOrdemservicos() {
        return this.ordemservicos;
    }

    public void setOrdemservicos(Set<Ordemservico> ordemservicos) {
        this.ordemservicos = ordemservicos;
    }

}
