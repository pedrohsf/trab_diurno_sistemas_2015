package business;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class VigenciaprecoId implements java.io.Serializable {

    private int idProduto;
    private Date dataInicio;

    public VigenciaprecoId() {
    }

    public VigenciaprecoId(int idProduto, Date dataInicio) {
        this.idProduto = idProduto;
        this.dataInicio = dataInicio;
    }

    @Column(name = "IdProduto", nullable = false)
    public int getIdProduto() {
        return this.idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    @Column(name = "DataInicio", nullable = false, length = 19)
    public Date getDataInicio() {
        return this.dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof VigenciaprecoId)) {
            return false;
        }
        VigenciaprecoId castOther = (VigenciaprecoId) other;

        return (this.getIdProduto() == castOther.getIdProduto())
                && ((this.getDataInicio() == castOther.getDataInicio()) || (this.getDataInicio() != null && castOther.getDataInicio() != null && this.getDataInicio().equals(castOther.getDataInicio())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getIdProduto();
        result = 37 * result + (getDataInicio() == null ? 0 : this.getDataInicio().hashCode());
        return result;
    }

}
