package business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "venda", catalog = "lojadiurno"
)
public class Venda implements java.io.Serializable {

    private Integer idVenda;
    private Cliente cliente;
    private Pagamento pagamento;
    private Vendedor vendedor;
    private Date dataVenda;
    private String observacao;
    private BigDecimal valorTotal;
    private BigDecimal desconto;
    private Set<Notafiscal> notafiscals = new HashSet<Notafiscal>(0);
    private List<Itemvenda> itemvendas = new ArrayList<Itemvenda>();

    public Venda() {
    }

    public Venda(Cliente cliente, Pagamento pagamento, Date dataVenda, BigDecimal valorTotal, BigDecimal desconto) {
        this.cliente = cliente;
        this.pagamento = pagamento;
        this.dataVenda = dataVenda;
        this.valorTotal = valorTotal;
        this.desconto = desconto;
    }

    public Venda(Cliente cliente, Pagamento pagamento, Vendedor vendedor, Date dataVenda, String observacao, BigDecimal valorTotal, BigDecimal desconto, Set<Notafiscal> notafiscals, List<Itemvenda> itemvendas) {
        this.cliente = cliente;
        this.pagamento = pagamento;
        this.vendedor = vendedor;
        this.dataVenda = dataVenda;
        this.observacao = observacao;
        this.valorTotal = valorTotal;
        this.desconto = desconto;
        this.notafiscals = notafiscals;
        this.itemvendas = itemvendas;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    
    @Column(name = "IdVenda", unique = true, nullable = false)
    public Integer getIdVenda() {
        return this.idVenda;
    }

    public void setIdVenda(Integer idVenda) {
        this.idVenda = idVenda;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdCliente", nullable = false)
    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdTipoPagamento", nullable = true)
    public Pagamento getPagamento() {
        return this.pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdVendedor", nullable = true)
    public Vendedor getVendedor() {
        return this.vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DataVenda", nullable = false, length = 19)
    public Date getDataVenda() {
        return this.dataVenda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    @Column(name = "Observacao", length = 65535)
    public String getObservacao() {
        return this.observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Column(name = "ValorTotal", nullable = true, precision = 10)
    public BigDecimal getValorTotal() {
        return this.valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    @Column(name = "Desconto", nullable = true, precision = 10)
    public BigDecimal getDesconto() {
        return this.desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "venda")
    public Set<Notafiscal> getNotafiscals() {
        return this.notafiscals;
    }

    public void setNotafiscals(Set<Notafiscal> notafiscals) {
        this.notafiscals = notafiscals;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "venda")
    public List<Itemvenda> getItemvendas() {
        return this.itemvendas;
    }

    public void setItemvendas(List<Itemvenda> itemvendas) {
        this.itemvendas = itemvendas;
    }

}
